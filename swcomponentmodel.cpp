/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "swcomponentmodel.h"

#include <QMessageBox>
#include <QDebug>

SWComponentModel::SWComponentModel()
{
    interfaces = NULL;
}

QString SWComponentModel::getInterfaceType(int port)
{
    if (interfaces == NULL)
    {
        //QMessageBox::information(this, "Error", "Interfaces not initialized.", QMessageBox::Ok);
        qDebug() << "Interfaces not initialized";
        return "";
    }
    if (port >= portInterfaceTypeIndex.size())
    {
        //QMessageBox::information(this, "Error", "Interface type is invalid.", QMessageBox::Ok);
        qDebug() << "Interface type is invalid";
        return "";
    }
    return interfaces->at(portInterfaceTypeIndex[port]).interfaceName;
}

void SWComponentModel::removePort(int portIndex)
{
    portNames.remove(portIndex);
    portType.remove(portIndex);
    portInterfaceTypeIndex.remove(portIndex);
}

