/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef BEHAVIOURMODEL_H
#define BEHAVIOURMODEL_H

#include <QString>
#include <QVector>
#include "interfacemodel.h"
#include "swcomponentmodel.h"

class behaviourModel
{
public:
    behaviourModel();
    QString behaviourName;
    int componentTypeIndex;
    QString getComponentType(void);                                 // i.e. Component's name
    QVector <QString> runnableNames;
    QVector <float> minimumStartIntervals;
    QVector < QVector <QString> > attributeNames;
    QVector < QVector <int> > attributePortIndex;
    QString getAttributePort(int runnable, int attribute);      // i.e. Port name from the component
    QVector < QVector <int> > attributeElementIndex;
    QString getAttributeElement(int runnable, int attribute);   // i.e. Data name from interface
    QVector < QString > eventType;
    QVector <float> tEventPeriod;
    QVector <QString> tEventRTEEvent;
    QVector <int> iEventPortIndex;
    QString getInvokeEventPort(int runnable);                   // i.e. Port name from the component
    QVector <int> iEventOperationIndex;                         // not used in current version since there is only 1 operation per interface
    QString getInvokeEventOperation(int runnable);              // i.e. Operation name from the interface

    void removeAttribute(int runnableIndex, int attributeIndex);

    QVector <InterfaceModel> *interfaces;
    QVector <SWComponentModel> *components;
};

#endif // BEHAVIOURMODEL_H
