/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "createinterfacedialog.h"
#include "ui_createinterfacedialog.h"

#include <QDebug>
#include <QMessageBox>

void Log(QString);

CreateInterfaceDialog::CreateInterfaceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateInterfaceDialog)
{
    ui->setupUi(this);

    QVector <int> indices;
    indices.push_back(1);
    indices.push_back(2);
    QVector < QStringList > strs;
    QStringList column;
    column << "boolean" << "uint8" << "uint16" << "uint32" << "sint8" << "sint16" << "sint32" << "float32" << "float64" << "void";
    strs.push_back(column);
    column.clear();
    column << "in" << "out";
    strs.push_back(column);

    myCombo = new ComboBoxDelegate(this, &indices, &strs);
    ui->tableView->setItemDelegate(myCombo);

    model = new QStandardItemModel(0, 3, this);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Data Element Name")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Data Type")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Data Direction")));
    ui->tableView->setModel(model);

    on_interfaceType_currentIndexChanged(0);
    editInterfaceName = "";
}

CreateInterfaceDialog::~CreateInterfaceDialog()
{
    delete ui;
}

void CreateInterfaceDialog::editInterface(InterfaceModel interface)
{
    editInterfaceName = interface.interfaceName;
    ui->interfaceName->setText(interface.interfaceName);
    int index;
    if (interface.interfaceType == "clientServer")
        index = 0;
    else
        index = 1;
    ui->interfaceType->setCurrentIndex(index);
    // Check if interface is used, don't allow user to change the interface type
    for (int cIter = 0; cIter < components->size(); cIter++)
    {
        for (int pIter = 0; pIter < components->at(cIter).portNames.size(); pIter++)
        {
            if (interfaces->at(components->at(cIter).portInterfaceTypeIndex.at(pIter)).interfaceName == interface.interfaceName)
            {
                ui->interfaceType->setEnabled(false);
                break;
            }
        }
    }

    if (index == 0)
    {
        ui->operationName->setText(interface.operationName);
    }
    model->removeRows(0, model->rowCount());            // Clear the model

    for (int dataIter = 0; dataIter < interface.dataNames.size(); dataIter++)
    {
        QStandardItem *name = new QStandardItem(QString(interface.dataNames.at(dataIter)));
        QStandardItem *type = new QStandardItem(QString(interface.dataType.at(dataIter)));
        QList<QStandardItem*> row;
        if (index == 0)
        {
            QStandardItem *direction = new QStandardItem(QString(interface.dataDirection.at(dataIter)));
            row << name << type << direction;
        }
        else
        {
            row << name << type;
        }
        model->appendRow(row);
    }
}

void CreateInterfaceDialog::setData(QVector <InterfaceModel> *interfaces, QVector<SWComponentModel> *components, QVector<behaviourModel> *behaviours)
{
    this->interfaces = interfaces;
    this->components = components;
    this->behaviours = behaviours;
}

InterfaceModel CreateInterfaceDialog::getInterface()
{
    InterfaceModel newInt;
    newInt.interfaceName = ui->interfaceName->text();
    newInt.interfaceType = ui->interfaceType->currentText();
    if (ui->interfaceType->currentIndex() == 0)
    {
        newInt.operationName = ui->operationName->text();
    }
    else
    {
        newInt.operationName = "";
    }
    for (int rowIter = 0; rowIter < model->rowCount(); rowIter++)
    {
        newInt.dataNames.push_back(model->data(model->index(rowIter, 0)).toString());
        newInt.dataType.push_back(model->data(model->index(rowIter, 1)).toString());
        if (ui->interfaceType->currentIndex() == 0)
        {
            newInt.dataDirection.push_back(model->data(model->index(rowIter, 2)).toString());
        }
    }

    // Log the new component
    Log("Getting interface " + ui->interfaceName->text());
    Log("\tInterface Type " + ui->interfaceType->currentText());
    if (ui->interfaceType->currentIndex() == 0)
    {
        Log("\tOperation Name " + ui->operationName->text());
    }
    for (int rowIter = 0; rowIter < model->rowCount(); rowIter++)
    {
        Log("\t\tData Name " + model->data(model->index(rowIter, 0)).toString());
        Log("\t\tData Type " + model->data(model->index(rowIter, 1)).toString());
        if (ui->interfaceType->currentIndex() == 0)
        {
            Log("\t\tData Direction " + model->data(model->index(rowIter, 2)).toString());
        }
    }
    return newInt;
}

void CreateInterfaceDialog::on_addButton_clicked()
{
    if (ui->interfaceType->currentIndex() == 0)
    {
        QStandardItem *name = new QStandardItem(QString(""));
        QStandardItem *type = new QStandardItem(QString("boolean"));
        QStandardItem *direction = new QStandardItem(QString("in"));
        QList<QStandardItem*> row;
        row << name << type << direction;
        model->appendRow(row);
    }
    else if (ui->interfaceType->currentIndex() == 1)
    {
        QStandardItem *name = new QStandardItem(QString(""));
        QStandardItem *type = new QStandardItem(QString("boolean"));
        QList<QStandardItem*> row;
        row << name << type;
        model->appendRow(row);
    }
}

void CreateInterfaceDialog::on_interfaceType_currentIndexChanged(int index)
{
    model->removeRows(0, model->rowCount());            // Clear the model

    if (index == 0)
    {
        ui->tableView->setColumnWidth(0,165);
        ui->tableView->setColumnWidth(1,120);
        ui->tableView->setColumnWidth(2,120);

        ui->label_3->setVisible(true);
        ui->operationName->setVisible(true);
    }
    else
    {
        ui->tableView->setColumnWidth(0,195);
        ui->tableView->setColumnWidth(1,195);
        ui->tableView->setColumnHidden(2, true);

        ui->label_3->setVisible(false);
        ui->operationName->setVisible(false);
    }
    on_addButton_clicked();
}

void CreateInterfaceDialog::on_removeButton_clicked()
{
    if (editInterfaceName != "")
        Log("Removing data item " + ui->interfaceType->currentText());
    if (model->rowCount() > 1)
    {
        // Check if the data item is used by some component
        bool askForBehaviours = false;
        QMessageBox::StandardButton reply1 = QMessageBox::No;
        if (ui->interfaceType->currentIndex() == 1)
        {
            for (int compIter = 0; compIter < components->size(); compIter++)
            {
                for (int portIter = 0; portIter < components->at(compIter).portNames.size(); portIter++)
                {
                    if ((*components)[compIter].getInterfaceType(portIter) == ui->interfaceName->text())
                    {
                        for (int behIter = 0; behIter < behaviours->size(); behIter++)
                        {
                            if ((*behaviours)[behIter].getComponentType() == components->at(compIter).componentName)
                            {
                                for (int runIter = 0; runIter < behaviours->at(behIter).runnableNames.size(); runIter++)
                                {
                                    for(int attIter = 0; attIter < behaviours->at(behIter).attributeNames.at(runIter).size(); attIter++)
                                    {
                                        if ((*behaviours)[behIter].attributeNames[runIter][attIter] != "serverCallPoint" && (*behaviours)[behIter].getAttributePort(runIter, attIter) == (*components)[compIter].portNames[portIter])
                                        {
                                            if (model->data(model->index(ui->tableView->currentIndex().row(), 0)).toString() == (*behaviours)[behIter].getAttributeElement(runIter, attIter))
                                            {
                                                askForBehaviours = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (askForBehaviours)
                                    {
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
                if (askForBehaviours)
                {
                    break;
                }
            }
            if (askForBehaviours)
            {
                reply1 = QMessageBox::question(this, "Error", "The data item \'" +
                                  model->data(model->index(ui->tableView->currentIndex().row(), 0)).toString() +
                                  "\' is used by some behaviours. Are you sure you want to remove this data item?", QMessageBox::Yes | QMessageBox::No);
                if (reply1 == QMessageBox::Yes)
                {
                    for (int compIter = 0; compIter < components->size(); compIter++)
                    {
                        for (int portIter = 0; portIter < components->at(compIter).portNames.size(); portIter++)
                        {
                            if ((*components)[compIter].getInterfaceType(portIter) == ui->interfaceName->text())
                            {
                                for (int behIter = 0; behIter < behaviours->size(); behIter++)
                                {
                                    if ((*behaviours)[behIter].getComponentType() == components->at(compIter).componentName)
                                    {
                                        for (int runIter = 0; runIter < behaviours->at(behIter).runnableNames.size(); runIter++)
                                        {
                                            bool deleted = false;
                                            for(int attIter = 0; attIter < behaviours->at(behIter).attributeNames.at(runIter).size(); attIter++)
                                            {
                                                if ((*behaviours)[behIter].attributeNames[runIter][attIter] != "serverCallPoint" && (*behaviours)[behIter].getAttributePort(runIter, attIter) == (*components)[compIter].portNames[portIter])
                                                {
                                                    if (model->data(model->index(ui->tableView->currentIndex().row(), 0)).toString() == (*behaviours)[behIter].getAttributeElement(runIter, attIter))
                                                    {
                                                        (*behaviours)[behIter].removeAttribute(runIter, attIter);
                                                        deleted = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (deleted)
                                            {
                                                // Update indices
                                                for(int attIter = 0; attIter < behaviours->at(behIter).attributeNames.at(runIter).size(); attIter++)
                                                {
                                                    if ((*behaviours)[behIter].attributeNames[runIter][attIter] != "serverCallPoint" && (*behaviours)[behIter].getAttributePort(runIter, attIter) == (*components)[compIter].portNames[portIter])
                                                    {
                                                        if ((*behaviours)[behIter].attributeElementIndex[runIter][attIter] >= ui->tableView->currentIndex().row())
                                                        {
                                                            (*behaviours)[behIter].attributeElementIndex[runIter][attIter]--;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    model->removeRows(ui->tableView->currentIndex().row(), 1);
                }
            }
        }
        if (!askForBehaviours)
        {
            reply1 = QMessageBox::question(this, "Confirm", "Are you sure you want to remove this data item?", QMessageBox::Yes|QMessageBox::No);
            if (reply1 == QMessageBox::Yes)
            {
                model->removeRows(ui->tableView->currentIndex().row(), 1);
            }
        }
    }
}

void CreateInterfaceDialog::done(int r)
{
    // Make sure that the interface model passed to the main editor is valid
    if(QDialog::Accepted == r)  // ok was pressed
    {
        // Check if the existing text fields are valid
        if (ui->interfaceName->text().isEmpty() || hasWhiteSpace(ui->interfaceName->text()) || ui->interfaceName->text().toInt() != 0)
        {
            QMessageBox::information(this, "Error", "Please type in a valid interface name. Whitespaces/numbered-names are not allowed.", QMessageBox::Ok);
            return;
        }
        if (ui->interfaceType->currentIndex() == 0 && (ui->operationName->text().isEmpty() || hasWhiteSpace(ui->operationName->text()) || ui->operationName->text().toInt() != 0))
        {
            QMessageBox::information(this, "Error", "Please type in a valid operation name. Whitespaces/numbered-names are not allowed.", QMessageBox::Ok);
            return;
        }
        for (int dataIter = 0; dataIter < model->rowCount(); dataIter++)
        {
            if (model->data(model->index(dataIter, 0)).toString().isEmpty() || model->data(model->index(dataIter, 0)).toString().toInt() != 0 || hasWhiteSpace(model->data(model->index(dataIter, 0)).toString()))
            {
                QMessageBox::information(this, "Error", "Please type in a valid data type name. Whitespaces/numbered-names are not allowed.", QMessageBox::Ok);
                return;
            }
            // Check if all the data names are unique
            for (int dataIter2 = 0; dataIter2 < model->rowCount(); dataIter2++)
            {
                if (dataIter2 != dataIter && model->data(model->index(dataIter2, 0)).toString() == model->data(model->index(dataIter, 0)).toString())
                {
                    QMessageBox::information(this, "Error", "Data element names must be unique.", QMessageBox::Ok);
                    return;
                }
            }
        }
        // Check if the name of the interface is unique
        for (int intIter = 0; intIter < interfaces->size(); intIter++)
        {
            if (interfaces->at(intIter).interfaceName == ui->interfaceName->text() && ui->interfaceName->text() != editInterfaceName)
            {
                QMessageBox::information(this, "Error", "The interface name must be unique.", QMessageBox::Ok);
                return;
            }
        }
        QDialog::done(r);
        return;
    }
    else    // cancel, close or exc was pressed
    {
         QDialog::done(r);
         return;
    }
}

bool CreateInterfaceDialog::hasWhiteSpace(QString s)
{
    std::string str = s.toStdString();
    for (int cIter = 0; cIter < s.size(); cIter++)
    {
        if (isspace(str[cIter]))
        {
            return true;
        }
    }
    return false;
}
