/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef MAINEDITOR_H
#define MAINEDITOR_H

#include <QGraphicsScene>
#include <QMainWindow>
#include <QStringListModel>
#include <QtXml>

#include "nodeseditor.h"
#include "interfacemodel.h"
#include "swcomponentmodel.h"
#include "behaviourmodel.h"

namespace Ui {
class MainEditor;
}

class MainEditor : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainEditor(QWidget *parent = 0);
    ~MainEditor();

private slots:

    void on_action_Quit_triggered();
    void on_addInterfaceButton_clicked();
    void on_addSWCButton_clicked();
    void on_actionOpen_ECU_Extract_triggered();
    void on_actionGenerate_SWC_files_triggered();
    void on_actionOpen_SWC_files_triggered();
    void on_editInterfaceButton_clicked();
    void on_editSWCButton_clicked();
    void on_removeInterfaceButton_clicked();
    void on_addBehaviour_clicked();
    void on_instantiateSWC_clicked();
    void on_removeSWCButton_clicked();
    void on_removeBehaviour_clicked();
    void on_editBehaviour_clicked();
    void on_actionGenerate_ECU_Extract_triggered();

private:
    Ui::MainEditor *ui;
    NodesEditor *nodesEditor;
    QGraphicsScene *scene;
    QVector<InterfaceModel> *interfaceModels;           // It has the same number of elements and arrangement as in ui->interfacesList
    QVector<SWComponentModel> *componentModels;         // It has the same number of elements and arrangement as in ui->componentsList
    QVector<behaviourModel> *behaviourModels;           // It has the same number of elements and arrangement as in ui->behavioursList
    // component and behaviours have 1 to 1 mapping, so the number of behaviours may not exceed the number of components
    QVector<QString> attrName, attrValue;               // Used for storing attributes for XML generation for ECU_EXTRACT

    void generateEcuExtract(QString rootPackageName, QString compositionName, QString extractName, QString file);

    // XML helpers
    QString generateUUID();
    QDomElement createSimpleElement(QDomDocument &doc, const QString &tag, QVector<QString> *attributeNames = NULL, QVector<QString> *attributeValues = NULL, const QString &value = NULL);
    void addAttribute(const QString &n, const QString &v);          // Used for storing attributes for XML generation for ECU_EXTRACT
    void clearAttributes();                                         // Used for storing attributes for XML generation for ECU_EXTRACT

};

#endif // MAINEDITOR_H
