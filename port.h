/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef PORT_H
#define PORT_H

#include <QGraphicsPixmapItem>

class SoftwareComponent;
class Interface;

class Port : public QGraphicsPixmapItem
{
public:
    enum { Type = QGraphicsItem::UserType + 1 };

    Port(QGraphicsItem *parent = 0);
    ~Port();

    void setNEBlock(SoftwareComponent*);
    void setName(const QString &n);
    void setType(int type, bool isRport, bool isService);
    void setPtr(quint64);
    int getPortType();
    const QString& getPortName() const { return name; }
    QVector<Interface*>& getConnections();
    SoftwareComponent* getBlock() const;
    quint64 getPtr();
    bool isRPort();
    bool isServicePort();
    bool isConnected(Port*);
    int type() const { return Type; }

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
    SoftwareComponent *m_block;
    QString name;
    bool isRPort_;
    int portType;
    bool isServicePort_;                        // not implemented in this version
    QGraphicsTextItem *label;
    QVector<Interface*> m_connections;          // Only used to clear interfaces from memory
    quint64 m_ptr;                      // Check if needed
};

#endif // PORT_H
