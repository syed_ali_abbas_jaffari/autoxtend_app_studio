/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "softwarecomponent.h"

#include <vector>
using namespace std;
#include <QtDebug>

#define HORIZ_MARGIN 90
#define VERT_MARGIN 30

SoftwareComponent::SoftwareComponent(QGraphicsItem *parent) : QGraphicsPathItem(parent)
{
    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemIsSelectable);
    width = 0;
    height = 0;
    name = new QGraphicsTextItem(this);
    subName = new QGraphicsTextItem(this);
    typeIndicator = new QGraphicsPixmapItem(this);
}

Port* SoftwareComponent::addPort(const QString &name, int ptr, int type, bool rPort, bool servicePort)
{
    Port *port = new Port(this);
    port->setName(name);
    port->setNEBlock(this);
    port->setPtr(ptr);
    port->setType(type, rPort, servicePort);

    return port;
}

void SoftwareComponent::setName(QString n)
{
    name->setPlainText(n);
}

QString SoftwareComponent::getName()
{
    return name->toPlainText();
}

void SoftwareComponent::setSubName(QString n)
{
    subName->setPlainText(n);
}

QString SoftwareComponent::getSubName()
{
    return subName->toPlainText();
}

void SoftwareComponent::save(QDataStream &ds)
{
    ds << pos();

    int count(0);

    foreach(QGraphicsItem *port_, childItems())
    {
        if (port_->type() != Port::Type)
            continue;

        count++;
    }

    ds << count;

    foreach(QGraphicsItem *port_, childItems())
    {
        if (port_->type() != Port::Type)
            continue;

        Port *port = (Port*) port_;
        ds << (quint64) port;
        ds << port->getPortName();
        ds << port->isRPort();
    }
}

void SoftwareComponent::load(QDataStream &ds, QMap<quint64, Port*> &portMap)
{
    QPointF p;
    ds >> p;
    setPos(p);
    int count;
    ds >> count;
    for (int i = 0; i < count; i++)
    {
        QString name;
        bool output;
        quint64 ptr;

        ds >> ptr;
        ds >> name;
        ds >> output;
        portMap[ptr] = addPort(name, ptr, 0, output);
    }
}

#include <QStyleOptionGraphicsItem>

void SoftwareComponent::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    QPen pen(Qt::black);
    pen.setWidth(3);
    painter->setPen(pen);
    if (isSelected()) {
        painter->setBrush(Qt::lightGray);
    } else {
        painter->setBrush(Qt::white);
    }

    painter->drawPath(path());
}

SoftwareComponent* SoftwareComponent::clone()
{
    SoftwareComponent *b = new SoftwareComponent(0);
    this->scene()->addItem(b);
    b->setName(name->toPlainText());
    if (subName->toPlainText() != "")
    {
        b->setSubName(subName->toPlainText());
    }

    foreach(QGraphicsItem *port_, childItems())
    {
        if (port_->type() == Port::Type)
        {
            Port *port = (Port*) port_;
            b->addPort(port->getPortName(), port->getPtr(), port->getPortType(), port->isRPort());
        }
    }

    return b;
}

QVector<Port*> SoftwareComponent::getPorts()
{
    QVector<Port*> res;
    foreach(QGraphicsItem *port_, childItems())
    {
        if (port_->type() == Port::Type)
            res.append((Port*) port_);
    }
    return res;
}

void SoftwareComponent::drawComponent()
{
    int numIPorts = 0;
    int numOPorts = 0;
    vector<int> oppositeWidths(childItems().size(), 0);
    int maxIOPorts;
    QFontMetrics fm(scene()->font());
    foreach(QGraphicsItem *port_, childItems())
    {
        if (port_->type() == Port::Type)
        {
            Port *po = (Port*) port_;
            if (po->isRPort())
            {
                oppositeWidths[numIPorts] += fm.width(po->getPortName());
                numIPorts++;
            }
            else
            {
                oppositeWidths[numOPorts] += fm.width(po->getPortName());
                numOPorts++;
            }
        }
    }
    if (numIPorts > numOPorts)
    {
        maxIOPorts = numIPorts;
    }
    else
    {
        maxIOPorts = numOPorts;
    }
    for (int i = 0; i < maxIOPorts; i++)
    {
        if (width < oppositeWidths[i])
        {
            width = oppositeWidths[i];
        }
    }
    if (width < fm.width(name->toPlainText()))
    {
            width = fm.width(name->toPlainText());
    }
    if (width < fm.width(subName->toPlainText()))
    {
            width = fm.width(subName->toPlainText());
    }

    int h = fm.height()*2;
    width += HORIZ_MARGIN;
    height += h*(maxIOPorts+2) - h/2;

    QPainterPath p;
    p.addRoundedRect(-width/2, -height/2, width, height, 2.5, 2.5);
    setPath(p);

    QPixmap typeImg;
    if (componentType == 0)
    {
        typeImg = QPixmap (":/components/app");
    }
    else if (componentType == 1)
    {
        typeImg = QPixmap (":/components/sa");
    }
    else if (componentType == 2)
    {
        typeImg = QPixmap (":/components/comp");
    }
    typeIndicator->setPixmap(typeImg);
    typeIndicator->setPos(width/2 - 20 - typeImg.width()/2, -height/2 + 20 - typeImg.height()/2);

    int y = -height / 2 + VERT_MARGIN + h;
    numIPorts = 0;
    numOPorts = 0;
    foreach(QGraphicsItem *port_, childItems()) {
        if (port_->type() != Port::Type)
            continue;

        Port *port = (Port*) port_;
        if (!port->isRPort())
        {
            port->setPos(width/2, y + numOPorts * h);
            numOPorts++;
        }
        else
        {
            port->setPos(-width/2, y + numIPorts * h);
            numIPorts++;
        }
    }
    this->name->setPos(-this->name->boundingRect().width()/2, -height/2);
    subName->setPos(this->name->pos().x() + this->name->boundingRect().width()/2 - subName->boundingRect().width()/2, this->name->pos().y() + h/2);
}

QVariant SoftwareComponent::itemChange(GraphicsItemChange change, const QVariant &value)
{

    Q_UNUSED(change);

    return value;
}

int SoftwareComponent::getComponentType() const
{
    return componentType;
}

void SoftwareComponent::setComponentType(int value)
{
    componentType = value;
}

