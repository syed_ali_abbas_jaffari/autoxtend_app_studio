/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "generateextractdialog.h"
#include "ui_generateextractdialog.h"

#include <QFileDialog>
#include <QMessageBox>

GenerateExtractDialog::GenerateExtractDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::generateextractdialog)
{
    ui->setupUi(this);

    // for testing
    ui->rootPackageName->setText("rte_simple");
    ui->compositionName->setText("NoComExtract");
    ui->extractName->setText("NoComExtractEcuExtract");
    ui->extractFileName->setText("test");
    ui->extractFileLocation->setText("/home/syed/PhD_Syed/branch/autosar/build-AutosarSWCEditor-Desktop_Qt_5_5_1_GCC_64bit-Debug");
}

GenerateExtractDialog::~GenerateExtractDialog()
{
    delete ui;
}

QStringList GenerateExtractDialog::getData()
{
    QStringList data;
    data << ui->rootPackageName->text() << ui->compositionName->text() << ui->extractName->text() << ui->extractFileName->text() << ui->extractFileLocation->text();
    return data;
}

void GenerateExtractDialog::on_pushButton_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Find Files"), QDir::currentPath());
    if (!directory.isEmpty())
    {
        ui->extractFileLocation->setText(directory);
    }
}

void GenerateExtractDialog::done(int r)
{
    // Make sure that the attribute passed to the internalBehaviourDialog is valid
    if(QDialog::Accepted == r)  // ok was pressed
    {
        if (ui->rootPackageName->text().isEmpty() || hasWhiteSpace(ui->rootPackageName->text()))
        {
            QMessageBox::information(this, "Error", "Not a valid root package name. White-spaces are not allowed.", QMessageBox::Ok);
            return;
        }
        if (ui->compositionName->text().isEmpty() || hasWhiteSpace(ui->compositionName->text()))
        {
            QMessageBox::information(this, "Error", "Not a valid composition name. White-spaces are not allowed.", QMessageBox::Ok);
            return;
        }
        if (ui->extractName->text().isEmpty() || hasWhiteSpace(ui->extractName->text()))
        {
            QMessageBox::information(this, "Error", "Not a valid extract name. White-spaces are not allowed.", QMessageBox::Ok);
            return;
        }
        if (ui->extractFileName->text().isEmpty() || hasWhiteSpace(ui->extractFileName->text()))
        {
            QMessageBox::information(this, "Error", "Not a valid extract file name. White-spaces are not allowed.", QMessageBox::Ok);
            return;
        }
        if (ui->extractFileLocation->text().isEmpty() || hasWhiteSpace(ui->extractFileLocation->text()))
        {
            QMessageBox::information(this, "Error", "Not a valid extract file location. White-spaces are not allowed.", QMessageBox::Ok);
            return;
        }
        QDialog::done(r);
        return;
    }
    else    // cancel, close or exc was pressed
    {
         QDialog::done(r);
         return;
    }
}

bool GenerateExtractDialog::hasWhiteSpace(QString s)
{
    std::string str = s.toStdString();
    for (int cIter = 0; cIter < s.size(); cIter++)
    {
        if (isspace(str[cIter]))
        {
            return true;
        }
    }
    return false;
}
