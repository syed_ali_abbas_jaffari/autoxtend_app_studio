/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef SOFTWARECOMPONENT_H
#define SOFTWARECOMPONENT_H

#include <QGraphicsPathItem>
#include <QGraphicsScene>
#include <QPen>
#include <QPainter>
#include "port.h"

class SoftwareComponent : public QGraphicsPathItem
{
public:
    enum { Type = QGraphicsItem::UserType + 3 };

    SoftwareComponent(QGraphicsItem *parent = 0);

    Port* addPort(const QString &name, int ptr = 0, int type = 0, bool rPort = false, bool servicePort = false);
    void save(QDataStream&);
    void load(QDataStream&, QMap<quint64, Port*> &portMap);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    SoftwareComponent* clone();
    void drawComponent();

    void setName(QString n);
    void setSubName(QString n);
    QString getName();
    QString getSubName();
    QVector<Port*> getPorts();

    int type() const { return Type; }

    int getComponentType() const;
    void setComponentType(int value);

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
    int width;
    int height;
    QGraphicsTextItem *name;
    QGraphicsTextItem *subName;
    QGraphicsPixmapItem *typeIndicator;
    int componentType;
};

#endif // SOFTWARECOMPONENT_H
