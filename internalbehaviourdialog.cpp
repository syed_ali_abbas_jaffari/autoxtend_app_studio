/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "internalbehaviourdialog.h"
#include "ui_internalbehaviourdialog.h"
#include "attributedialog.h"

#include <QMessageBox>
#include <QDebug>

void Log(QString);

InternalBehaviourDialog::InternalBehaviourDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InternalBehaviourDialog)
{
    ui->setupUi(this);

    ui->runnableAttributes->setEnabled(false);
    ui->addAttribute->setEnabled(false);
    ui->removeAttribute->setEnabled(false);
    ui->editAttribute->setEnabled(false);
    ui->eventType->setEnabled(false);
    ui->eventCol1->setEnabled(false);
    ui->eventCol2->setEnabled(false);

    editBehaviourName = "";
}

InternalBehaviourDialog::~InternalBehaviourDialog()
{
    delete ui;
}

void InternalBehaviourDialog::setData(QVector<InterfaceModel> *interfaces, QVector<SWComponentModel> *components, QVector <behaviourModel> *behaviours)
{
    this->components = components;
    this->interfaces = interfaces;
    this->behaviours = behaviours;

    for (int cIter = 0; cIter < components->size(); cIter++)
    {
        ui->componentType->addItem(components->at(cIter).componentName);
    }
}

behaviourModel InternalBehaviourDialog::getBehaviour()
{
    behaviourModel newBehaviour;
    newBehaviour.interfaces = interfaces;
    newBehaviour.components = components;
    newBehaviour.behaviourName = ui->internalBehaviourName->text();
    Log ("Getting behaviour " + ui->internalBehaviourName->text());
    newBehaviour.componentTypeIndex = ui->componentType->currentIndex();
    Log ("\tBehaviour Component " + ui->componentType->currentText());
    for (int rIter = 0; rIter < runnableNames.size(); rIter++)
    {
        newBehaviour.runnableNames.push_back(runnableNames[rIter]);
        Log("\t\tRunnable Name " + runnableNames[rIter]);
        newBehaviour.minimumStartIntervals.push_back(minimumStartIntervals[rIter]);
        Log ("\t\tMinimum Start Interval " + QString::number(minimumStartIntervals[rIter]));
        newBehaviour.eventType.push_back(eventTypes[rIter]);
        Log ("\t\tEvent Type " + eventTypes[rIter]);
        if (eventTypes[rIter] == "None")
        {
            newBehaviour.tEventPeriod.push_back(0.0f);
            newBehaviour.tEventRTEEvent.push_back("");
            newBehaviour.iEventPortIndex.push_back(0);
        }
        else if (eventTypes[rIter] == "timingEvent")
        {
            Log ("\t\tEvent Period " + eventColumn1[rIter]);
            newBehaviour.tEventPeriod.push_back(eventColumn1[rIter].toFloat());
            Log ("\t\tEvent RTEEvent " + eventColumn2[rIter]);
            newBehaviour.tEventRTEEvent.push_back(eventColumn2[rIter]);
            newBehaviour.iEventPortIndex.push_back(0);
        }
        else if (eventTypes[rIter] == "operationInvokedEvent" && eventColumn1[rIter] != "")
        {
            newBehaviour.tEventPeriod.push_back(0.0f);
            newBehaviour.tEventRTEEvent.push_back("");
            for (int pIter = 0; pIter < components->at(ui->componentType->currentIndex()).portNames.size(); pIter++)
            {
                if (components->at(ui->componentType->currentIndex()).portNames[pIter] == eventColumn1[rIter])
                {
                    newBehaviour.iEventPortIndex.push_back(pIter);
                    Log("\t\tEvent Port Name " + eventColumn1[rIter]);
                }
            }
        }
        QVector <QString> attributeNamesPerRunnable;
        QVector <int> attributeSWCPortsPerRunnable;
        QVector <int> attributeElementsPerRunnable;
        for (int attIter = 0; attIter < attributeNames[rIter].size(); attIter++)
        {
            attributeNamesPerRunnable.push_back(attributeNames[rIter][attIter]);
            Log("\t\t\tAttribute Name " + attributeNames[rIter][attIter]);
            for (int pIter = 0; pIter < components->at(ui->componentType->currentIndex()).portNames.size(); pIter++)
            {
                if (components->at(ui->componentType->currentIndex()).portNames[pIter] == attributeSWCPorts[rIter][attIter])
                {
                    attributeSWCPortsPerRunnable.push_back(pIter);
                    Log("\t\t\tAttribute Port " + attributeSWCPorts[rIter][attIter]);
                    for (int iIter = 0; iIter < interfaces->size(); iIter++)
                    {
                        if (interfaces->at(iIter).interfaceName == (*components)[ui->componentType->currentIndex()].getInterfaceType(pIter))
                        {
                            bool specified = false;
                            for (int dIter = 0; dIter < interfaces->at(iIter).dataNames.size(); dIter++)
                            {
                                if (attributeElements[rIter][attIter] == interfaces->at(iIter).dataNames[dIter])
                                {
                                    attributeElementsPerRunnable.push_back(dIter);
                                    specified = true;
                                    Log("\t\t\tAttribute Element " + attributeElements[rIter][attIter]);
                                    break;
                                }
                            }
                            if (!specified)
                            {
                                attributeElementsPerRunnable.push_back(-1);
                                Log("\t\t\tAttribute Element -1");
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
        Q_ASSERT(attributeElementsPerRunnable.size() == attributeNamesPerRunnable.size());
        Q_ASSERT(attributeSWCPortsPerRunnable.size() == attributeNamesPerRunnable.size());
        newBehaviour.attributeNames.push_back(attributeNamesPerRunnable);
        newBehaviour.attributePortIndex.push_back(attributeSWCPortsPerRunnable);
        newBehaviour.attributeElementIndex.push_back(attributeElementsPerRunnable);
    }
    return newBehaviour;
}

void InternalBehaviourDialog::editBehaviour(behaviourModel *behav)
{
    editBehaviourName = behav->behaviourName;
    // Extract information from behaviour
    for (int rIter = 0; rIter < behav->runnableNames.size(); rIter++)
    {
        runnableNames.push_back(behav->runnableNames[rIter]);
        minimumStartIntervals.push_back(behav->minimumStartIntervals[rIter]);
        eventTypes.push_back(behav->eventType[rIter]);
        if (eventTypes[rIter] == "None")
        {
            eventColumn1.push_back("");
            eventColumn2.push_back("");
        }
        else if (eventTypes[rIter] == "timingEvent")
        {
            eventColumn1.push_back(QString::number(behav->tEventPeriod[rIter], 'f'));
            eventColumn2.push_back(behav->tEventRTEEvent[rIter]);
        }
        else if (eventTypes[rIter] == "operationInvokedEvent")
        {
            eventColumn1.push_back(components->at(behav->componentTypeIndex).portNames[behav->iEventPortIndex[rIter]]);
            eventColumn2.push_back(interfaces->at(components->at(behav->componentTypeIndex).portInterfaceTypeIndex[behav->iEventPortIndex[rIter]]).operationName);
        }
        QVector <QString> attributeNamesPerRunnable;
        QVector <QString> attributeSWCPortsPerRunnable;
        QVector <QString> attributeElementsPerRunnable;
        for (int attIter = 0; attIter < behav->attributeNames[rIter].size(); attIter++)
        {
            attributeNamesPerRunnable.push_back(behav->attributeNames[rIter][attIter]);
            qDebug() << "Attribute name" << behav->attributeNames[rIter][attIter];
            attributeSWCPortsPerRunnable.push_back(components->at(behav->componentTypeIndex).portNames[behav->attributePortIndex[rIter][attIter]]);
            qDebug() << "Port name " << components->at(behav->componentTypeIndex).portNames[behav->attributePortIndex[rIter][attIter]];
            if (behav->attributeElementIndex[rIter][attIter] != -1)
            {
                attributeElementsPerRunnable.push_back(interfaces->at(components->at(behav->componentTypeIndex).portInterfaceTypeIndex[behav->attributePortIndex[rIter][attIter]]).dataNames[behav->attributeElementIndex[rIter][attIter]]);
                qDebug() << "Element name" << interfaces->at(components->at(behav->componentTypeIndex).portInterfaceTypeIndex[behav->attributePortIndex[rIter][attIter]]).dataNames[behav->attributeElementIndex[rIter][attIter]];
            }
            else if (behav->attributeNames[rIter][attIter] == "serverCallPoint")
            {
                attributeElementsPerRunnable.push_back(interfaces->at(components->at(behav->componentTypeIndex).portInterfaceTypeIndex[behav->attributePortIndex[rIter][attIter]]).operationName);
                qDebug() << "Operation name " << interfaces->at(components->at(behav->componentTypeIndex).portInterfaceTypeIndex[behav->attributePortIndex[rIter][attIter]]).operationName;
            }
            else
            {
                attributeElementsPerRunnable.push_back("*");
                qDebug() << "Element names *";
            }
        }
        attributeNames.push_back(attributeNamesPerRunnable);
        attributeSWCPorts.push_back(attributeSWCPortsPerRunnable);
        attributeElements.push_back(attributeElementsPerRunnable);
    }

    // Update ui elements
    ui->internalBehaviourName->setText(behav->behaviourName);
    ui->componentType->setCurrentIndex(behav->componentTypeIndex);
    for (int rIter = 0; rIter < behav->runnableNames.size(); rIter++)
    {
        ui->runnablesList->addItem(runnableNames[rIter]);
    }
}

void InternalBehaviourDialog::on_addAttribute_clicked()
{
    AttributeDialog newDialog;
    newDialog.setModal(true);
    newDialog.setData(interfaces, &(*components)[ui->componentType->currentIndex()]);
    if (newDialog.exec() == QDialog::Accepted)
    {
        int runnableIndex = ui->runnablesList->currentRow();
        QString a, b, c;
        newDialog.getData(&a, &b, &c);
        attributeNames[runnableIndex].push_back(a);
        attributeSWCPorts[runnableIndex].push_back(b);
        attributeElements[runnableIndex].push_back(c);

        ui->runnableAttributes->setEnabled(true);
        enableAttributeTable();
        int rows = ui->runnableAttributes->rowCount();
        ui->runnableAttributes->setRowCount(rows + 1);
        ui->runnableAttributes->setItem(rows, 0, new QTableWidgetItem(a));
        ui->runnableAttributes->setItem(rows, 1, new QTableWidgetItem(b));
        ui->runnableAttributes->setItem(rows, 2, new QTableWidgetItem(c));
        ui->eventType->setEnabled(true);
    }
}

void InternalBehaviourDialog::done(int r)
{
    // Make sure that the interface model passed to the main editor is valid
    if(QDialog::Accepted == r)  // ok was pressed
    {
        for (int behIter = 0; behIter < behaviours->size(); behIter++)
        {
            if (ui->componentType->currentText() == (*behaviours)[behIter].getComponentType() && editBehaviourName == "")
            {
                QMessageBox::information(this, "Error", "Internal behaviour of the component \'" + ui->componentType->currentText() + "\' already exists. Please specify the component for which the behaviour is not yet defined.", QMessageBox::Ok);
                return;
            }
            if (ui->internalBehaviourName->text() == behaviours->at(behIter).behaviourName && editBehaviourName == "")
            {
                QMessageBox::information(this, "Error", "Internal behaviour name must be unique.", QMessageBox::Ok);
                return;
            }
        }
        if (ui->internalBehaviourName->text().isEmpty() || hasWhiteSpace(ui->internalBehaviourName->text()) || ui->internalBehaviourName->text().toInt() != 0)
        {
            QMessageBox::information(this, "Error", "Please type in a valid behaviour name. Whitespaces/numbered-names are not allowed.", QMessageBox::Ok);
            return;
        }
        if (runnableNames.size() == 0)
        {
            QMessageBox::information(this, "Error", "Number of runnables must be greater than 0.", QMessageBox::Ok);
            return;
        }
        for (int runIter = 0; runIter < runnableNames.size(); runIter++)
        {
            if (attributeNames[runIter].size() == 0 && eventTypes[runIter] != "operationInvokedEvent")
            {
                QMessageBox::information(this, "Error", "Number of attributes for each runnable must be greater than 0", QMessageBox::Ok);
                return;
            }
            // Check if all the attributes of all the runnables are unique
            for (int attIter = 0; attIter < attributeNames[runIter].size(); attIter++)
            {
                for (int attIter2 = 0; attIter2 < attributeNames[runIter].size(); attIter2++)
                {
                    if (attIter != attIter2 && attributeNames[runIter][attIter] == attributeNames[runIter][attIter2])
                    {
                        QMessageBox::information(this, "Error", "Attribute names of a runnable must be unique.", QMessageBox::Ok);
                        return;
                    }
                }
            }
        }
        QDialog::done(r);
        return;
    }
    else    // cancel, close or exc was pressed
    {
         QDialog::done(r);
         return;
    }
}

void InternalBehaviourDialog::on_addRunnable_clicked()
{
    if (ui->runnableName->text().isEmpty() || hasWhiteSpace(ui->runnableName->text()) || ui->runnableName->text().toInt() != 0)
    {
        QMessageBox::information(this, "Error", "Please type in a valid runnable name. Whitespaces/numbered-names are not allowed.", QMessageBox::Ok);
        return;
    }
    bool floatConverted;
    ui->runnableName->text().toFloat(&floatConverted);
    if (ui->minimumStartInterval->text().isEmpty() || hasWhiteSpace(ui->runnableName->text()) || floatConverted == true)
    {
        QMessageBox::information(this, "Error", "Please type in a valid minimum start interval. Only floating point values are allowed.", QMessageBox::Ok);
        return;
    }
    for (int runIter = 0; runIter < runnableNames.size(); runIter++)
    {
        if (runnableNames.at(runIter) == ui->runnableName->text())
        {
            QMessageBox::information(this, "Error", "Runnable names must be unique.", QMessageBox::Ok);
            return;
        }
    }
    runnableNames.push_back(ui->runnableName->text());
    minimumStartIntervals.push_back(ui->minimumStartInterval->text().toFloat());
    ui->runnablesList->addItem(ui->runnableName->text());

    QVector <QString> newEntry;
    attributeNames.push_back(newEntry);
    attributeSWCPorts.push_back(newEntry);
    attributeElements.push_back(newEntry);

    eventTypes.push_back("None");
    eventColumn1.push_back("");
    eventColumn2.push_back("");
    ui->runnablesList->setCurrentRow(runnableNames.size()-1);
}

bool InternalBehaviourDialog::hasWhiteSpace(QString s)
{
    std::string str = s.toStdString();
    for (int cIter = 0; cIter < s.size(); cIter++)
    {
        if (isspace(str[cIter]))
        {
            return true;
        }
    }
    return false;
}

void InternalBehaviourDialog::on_runnablesList_currentRowChanged(int currentRow)
{
    // Load attributes and events of current in the widgets
    if (currentRow != -1)
    {
        ui->runnableAttributes->setEnabled(true);
        ui->runnableAttributes->clear();
        enableAttributeTable();
        ui->runnableAttributes->setRowCount(0);
        for (int rowIter = 0; rowIter < attributeNames[currentRow].size(); rowIter++)
        {
            int rows = ui->runnableAttributes->rowCount();
            ui->runnableAttributes->setRowCount(rows + 1);
            ui->runnableAttributes->setItem(rows, 0, new QTableWidgetItem(attributeNames[currentRow][rowIter]));
            ui->runnableAttributes->setItem(rows, 1, new QTableWidgetItem(attributeSWCPorts[currentRow][rowIter]));
            ui->runnableAttributes->setItem(rows, 2, new QTableWidgetItem(attributeElements[currentRow][rowIter]));
        }

        ui->addAttribute->setEnabled(true);
        ui->removeAttribute->setEnabled(true);
        ui->editAttribute->setEnabled(true);

        ui->eventType->setEnabled(true);
        ui->minimumStartInterval->setText(QString::number(minimumStartIntervals[currentRow], 'f'));
        if (eventTypes[currentRow] == "operationInvokedEvent" && eventColumn1[currentRow] == "")
        {
            eventTypes[currentRow] = "None";
        }
        if (eventTypes[currentRow] == "None")
        {
            ui->eventType->setEnabled(true);
            ui->eventType->setCurrentIndex(0);
            ui->eventCol1->setEnabled(false);
            ui->eventCol2->setEnabled(false);
        }
        else if (eventTypes[currentRow] == "timingEvent")
        {
            ui->eventType->setCurrentIndex(1);
            ui->eventCol1->setEnabled(true);
            ui->eventCol2->setEnabled(true);
            ui->eventCol1->setCurrentIndex(0);
            ui->eventCol2->setCurrentIndex(0);
            ui->eventPeriod->setText(eventColumn1[currentRow]);
            ui->eventRTEEvent->setText(eventColumn2[currentRow]);
        }
        else if (eventTypes[currentRow] == "operationInvokedEvent")
        {
            ui->eventType->setCurrentIndex(2);
            ui->eventCol1->setEnabled(true);
            ui->eventCol2->setEnabled(true);
            ui->eventCol1->setCurrentIndex(1);
            ui->eventCol2->setCurrentIndex(1);
            populateEventPorts();
            for (int pIter = 0; pIter < ui->eventPort->count(); pIter++)
            {
                if (eventColumn1[currentRow] == ui->eventPort->itemText(pIter))
                {
                    ui->eventPort->setCurrentIndex(pIter);
                    break;
                }
            }
            ui->eventRTEEvent->setText(eventColumn2[currentRow]);
        }
    }
    else
    {
        ui->runnableAttributes->setEnabled(false);
        ui->addAttribute->setEnabled(false);
        ui->removeAttribute->setEnabled(false);
        ui->editAttribute->setEnabled(false);
        ui->eventType->setEnabled(false);
        ui->eventCol1->setEnabled(false);
        ui->eventCol2->setEnabled(false);
    }
}

void InternalBehaviourDialog::on_eventType_currentIndexChanged(int index)
{
    if (index == 0)
    {
        ui->eventCol1->setEnabled(false);
        ui->eventCol2->setEnabled(false);
    }
    else if (index == 1)
    {
        ui->eventCol1->setEnabled(true);
        ui->eventCol2->setEnabled(true);
        ui->eventCol1->setCurrentIndex(0);
        ui->eventCol2->setCurrentIndex(0);
    }
    else if (index == 2)
    {
        ui->eventCol1->setEnabled(true);
        ui->eventCol1->setCurrentIndex(1);
        ui->eventCol2->setEnabled(true);
        ui->eventCol2->setCurrentIndex(1);
        populateEventPorts();
        if (ui->eventPort->count() == 0)
        {
            eventColumn1[ui->runnablesList->currentRow()] = "";
            eventColumn2[ui->runnablesList->currentRow()] = "";
        }
    }
    eventTypes[ui->runnablesList->currentRow()] = ui->eventType->currentText();
}

void InternalBehaviourDialog::on_eventPort_currentTextChanged(const QString &arg1)
{
    eventColumn1[ui->runnablesList->currentRow()] = arg1;
}

void InternalBehaviourDialog::on_eventOperation_currentTextChanged(const QString &arg1)
{
    eventColumn2[ui->runnablesList->currentRow()] = arg1;
}

void InternalBehaviourDialog::on_eventPeriod_editingFinished()
{
    bool floatConverted;
    ui->eventPeriod->text().toFloat(&floatConverted);
    if (ui->eventPeriod->text().isEmpty() || hasWhiteSpace(ui->eventPeriod->text()) || floatConverted == false)
    {
        QMessageBox::information(this, "Error", "Event period must be a float.", QMessageBox::Ok);
    }
    eventColumn1[ui->runnablesList->currentRow()] = ui->eventPeriod->text();
}

void InternalBehaviourDialog::on_eventRTEEvent_editingFinished()
{
    if (ui->eventRTEEvent->text().isEmpty() || hasWhiteSpace(ui->eventRTEEvent->text()) || ui->eventRTEEvent->text().toInt() != 0)
    {
        QMessageBox::information(this, "Error", "RTEEvent must be a valid name. Whitespaces/numbered-names are not allowed.", QMessageBox::Ok);
    }
    eventColumn2[ui->runnablesList->currentRow()] = ui->eventRTEEvent->text();
}

void InternalBehaviourDialog::enableAttributeTable()
{
    ui->runnableAttributes->setColumnCount(3);
    QStringList list;
    list << "Attribute" << "SWC Port" << "Element";
    ui->runnableAttributes->setHorizontalHeaderLabels(list);
    ui->runnableAttributes->setColumnWidth(0, 163);
    ui->runnableAttributes->setColumnWidth(1, 163);
    ui->runnableAttributes->setColumnWidth(2, 163);
}

void InternalBehaviourDialog::populateEventPorts()
{
    ui->eventPort->clear();
    for (int pIter = 0; pIter < components->at(ui->componentType->currentIndex()).portNames.size(); pIter++)
    {
        if (components->at(ui->componentType->currentIndex()).portType[pIter] == "server")
            ui->eventPort->addItem(components->at(ui->componentType->currentIndex()).portNames[pIter]);
    }
}

void InternalBehaviourDialog::populateEventOperations()
{
    ui->eventOperation->clear();
    for (int pIter = 0; pIter < components->at(ui->componentType->currentIndex()).portNames.size(); pIter++)
    {
        if (components->at(ui->componentType->currentIndex()).portNames[pIter] == ui->eventPort->currentText())
        {
            // look for interface
            for (int iIter = 0; iIter < interfaces->size(); iIter++)
            {
                if (interfaces->at(iIter).interfaceName == (*components)[ui->componentType->currentIndex()].getInterfaceType(pIter))
                {
                    ui->eventOperation->addItem(interfaces->at(iIter).operationName);
                    break;
                }
            }
            break;
        }
    }
}

void InternalBehaviourDialog::on_eventPort_currentIndexChanged(int index)
{
    Q_UNUSED(index);
    populateEventOperations();
}

void InternalBehaviourDialog::on_editAttribute_clicked()
{
    AttributeDialog newDialog;
    newDialog.setModal(true);
    newDialog.setData(interfaces, &(*components)[ui->componentType->currentIndex()]);
    int editRow = ui->runnableAttributes->currentRow();
    newDialog.editAttribute(ui->runnableAttributes->item(editRow, 0)->text(), ui->runnableAttributes->item(editRow, 1)->text(), ui->runnableAttributes->item(editRow, 2)->text());
    if (newDialog.exec() == QDialog::Accepted)
    {
        int runnableIndex = ui->runnablesList->currentRow();
        QString a, b, c;
        newDialog.getData(&a, &b, &c);
        attributeNames[runnableIndex].erase(attributeNames[runnableIndex].begin() + editRow);
        attributeNames[runnableIndex].insert(attributeNames[runnableIndex].begin() + editRow, a);
        attributeSWCPorts[runnableIndex].erase(attributeSWCPorts[runnableIndex].begin() + editRow);
        attributeSWCPorts[runnableIndex].insert(attributeSWCPorts[runnableIndex].begin() + editRow, b);
        attributeElements[runnableIndex].erase(attributeElements[runnableIndex].begin() + editRow);
        attributeElements[runnableIndex].insert(attributeElements[runnableIndex].begin() + editRow, c);

        ui->runnableAttributes->setItem(editRow, 0, new QTableWidgetItem(a));
        ui->runnableAttributes->setItem(editRow, 1, new QTableWidgetItem(b));
        ui->runnableAttributes->setItem(editRow, 2, new QTableWidgetItem(c));
    }
}

void InternalBehaviourDialog::on_removeAttribute_clicked()
{
    if (editBehaviourName != "")
        Log ("Removing Attribute " + attributeNames[ui->runnablesList->currentRow()][ui->runnableAttributes->currentRow()]);
    if (ui->runnableAttributes->currentRow() != -1)
    {
        QMessageBox::StandardButton reply1;
        int editRow = ui->runnableAttributes->currentRow();
        reply1 = QMessageBox::question(this, "Confirm", "Are you sure you want to remove the selected attribute?", QMessageBox::Yes|QMessageBox::No);
        if (reply1 == QMessageBox::Yes)
        {
            int runnableIndex = ui->runnablesList->currentRow();
            attributeNames[runnableIndex].erase(attributeNames[runnableIndex].begin() + editRow);
            attributeSWCPorts[runnableIndex].erase(attributeSWCPorts[runnableIndex].begin() + editRow);
            attributeElements[runnableIndex].erase(attributeElements[runnableIndex].begin() + editRow);

            ui->runnableAttributes->removeRow(editRow);
        }
    }
}

void InternalBehaviourDialog::on_removeRunnable_clicked()
{
    if (editBehaviourName != "")
        Log ("Removing Runnable " + runnableNames[ui->runnablesList->currentRow()]);
    if (ui->runnablesList->currentRow() != -1)
    {
        runnableNames.remove(ui->runnablesList->currentRow());
        minimumStartIntervals.remove(ui->runnablesList->currentRow());
        ui->runnablesList->takeItem(ui->runnablesList->currentRow());

        attributeNames.remove(ui->runnablesList->currentRow());
        attributeSWCPorts.remove(ui->runnablesList->currentRow());
        attributeElements.remove(ui->runnablesList->currentRow());

        eventTypes.remove(ui->runnablesList->currentRow());
        eventColumn1.remove(ui->runnablesList->currentRow());
        eventColumn2.remove(ui->runnablesList->currentRow());
        /*if (runnableNames.size() != 0)
            ui->runnablesList->setCurrentRow(0);*/
    }
}
