#-------------------------------------------------
#
# Project created by QtCreator 2015-11-28T00:40:43
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AutosarSWCEditor
TEMPLATE = app

LIBS += -luuid


SOURCES += main.cpp\
        maineditor.cpp \
    nodeseditor.cpp \
    softwarecomponent.cpp \
    port.cpp \
    interface.cpp \
    createinterfacedialog.cpp \
    comboboxdelegate.cpp \
    interfacemodel.cpp \
    createswcdialog.cpp \
    internalbehaviourdialog.cpp \
    attributedialog.cpp \
    swcomponentmodel.cpp \
    behaviourmodel.cpp \
    generateextractdialog.cpp

HEADERS  += maineditor.h \
    nodeseditor.h \
    softwarecomponent.h \
    port.h \
    interface.h \
    createinterfacedialog.h \
    comboboxdelegate.h \
    interfacemodel.h \
    createswcdialog.h \
    internalbehaviourdialog.h \
    attributedialog.h \
    swcomponentmodel.h \
    behaviourmodel.h \
    logger.h \
    generateextractdialog.h

FORMS    += maineditor.ui \
    createinterfacedialog.ui \
    createswcdialog.ui \
    internalbehaviourdialog.ui \
    attributedialog.ui \
    generateextractdialog.ui

RESOURCES += \
    resources.qrc
