/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "nodeseditor.h"

#include <QtDebug>

#define PATH_MODIFICATION_TOLERANCE 4

NodesEditor::NodesEditor(QObject *parent) :
    QObject(parent)
{
    interface = 0;
}

void NodesEditor::install(QGraphicsScene *s)
{
    s->installEventFilter(this);
    scene = s;
}

QGraphicsItem* NodesEditor::itemAt(const QPointF &pos)
{
    QList<QGraphicsItem*> items = scene->items(QRectF(pos - QPointF(1,1), QSize(3,3)));

    foreach(QGraphicsItem *item, items)
        if (item->type() > QGraphicsItem::UserType)
            return item;

    return 0;
}

bool NodesEditor::eventFilter(QObject *o, QEvent *e)
{
    QGraphicsSceneMouseEvent *me = (QGraphicsSceneMouseEvent*) e;

    switch ((int) e->type())
    {
    case QEvent::GraphicsSceneMousePress:
    {

        switch ((int) me->button())
        {
        case Qt::LeftButton:
        {
            QGraphicsItem *item = itemAt(me->scenePos());
            if (item && item->type() == Port::Type)
            {
                interface = new Interface(0);
                scene->addItem(interface);
                interface->setPort1((Port*) item);
                interface->setPos1(item->scenePos());
                interface->setPos2(me->scenePos());
                interface->updatePath();

                return true;
            } else if (item && item->type() == SoftwareComponent::Type)
            {
                /*QVector<Port*> ports = ((SoftwareComponent*)item)->getPorts();
                for (int pIter = 0; pIter < ports.size(); pIter++)
                {
                    QVector <Interface*> interfaces = ports[pIter]->getConnections();
                    if (interfaces.size() != 0)
                    {
                        for (int iIter = 0; iIter < interfaces.size(); iIter++)
                        {
                            interfaces[iIter]->setModifyPath(true);
                        }
                    }
                }*/
            } else if (item && item->type() == Interface::Type) {
                bool gotTheLine = false;
                QPainterPath path;
                path = ((Interface*) item)->path();
                for (int elIter = 1; elIter < path.elementCount()-2; elIter++)
                {
                    if (elIter == 1 || elIter == 3)
                    {
                        if (me->scenePos().x() < path.elementAt(elIter).x + PATH_MODIFICATION_TOLERANCE &&
                                me->scenePos().x() > path.elementAt(elIter).x - PATH_MODIFICATION_TOLERANCE)     // Mouse click x is close to middle element
                        {
                            int min = path.elementAt(elIter).y;
                            int max = path.elementAt(elIter+1).y;
                            if (path.elementAt(elIter).y > path.elementAt(elIter+1).y)
                            {
                               min = path.elementAt(elIter+1).y;
                               max = path.elementAt(elIter).y;
                            }
                            if (me->scenePos().y() < max && me->scenePos().y() > min)
                            {
                                interface = ((Interface*) item);
                                interface->setModifyPath(elIter);
                                gotTheLine = true;
                                break;
                            }
                        }
                    }
                    else if (me->scenePos().y() < path.elementAt(elIter).y + PATH_MODIFICATION_TOLERANCE &&
                             me->scenePos().y() > path.elementAt(elIter).y - PATH_MODIFICATION_TOLERANCE)     // Mouse click x is close to middle element
                    {
                        int min = path.elementAt(elIter).x;
                        int max = path.elementAt(elIter+1).x;
                        if (path.elementAt(elIter).x > path.elementAt(elIter+1).x)
                        {
                           min = path.elementAt(elIter+1).x;
                           max = path.elementAt(elIter).x;
                        }
                        if (me->scenePos().x() < max && me->scenePos().x() > min)
                        {
                            interface = ((Interface*) item);
                            interface->setModifyPath(elIter);
                            gotTheLine = true;
                            break;
                        }
                    }
                }
                if (!gotTheLine)
                    qDebug() << "Error: It is a known bug. Can be fixed after tracking this error message.";
            }
            break;
        }
        case Qt::RightButton:
        {
            QGraphicsItem *item = itemAt(me->scenePos());
            if (item && (item->type() == Interface::Type || item->type() == SoftwareComponent::Type))
                delete item;
            break;
        }
        }
    }
    case QEvent::GraphicsSceneMouseMove:
    {
        if (interface)
        {
            if (interface->getModifyPath() == 1)
            {
                interface->setFirstX(me->scenePos().x());
            }
            else if (interface->getModifyPath() == 2)
            {
                interface->setSecondY(me->scenePos().y());
            }
            else if (interface->getModifyPath() == 3)
            {
                interface->setThirdX(me->scenePos().x());
            }
            else
            {
                interface->setPos2(me->scenePos());
            }
            interface->updatePath();
            return true;
        }
        break;
    }
    case QEvent::GraphicsSceneMouseRelease:
    {
        if (interface && me->button() == Qt::LeftButton)
        {
            QGraphicsItem *item = itemAt(me->scenePos());
            if (item && item->type() == Port::Type)
            {
                Port *port1 = interface->port1();
                Port *port2 = (Port*) item;

                if (port1->getBlock() != port2->getBlock() && port1->isRPort() != port2->isRPort() && !port1->isConnected(port2) && port1->getPortType() == port2->getPortType())
                {
                    if (!port1->isRPort())
                    {
                        interface->setPos1(port2->scenePos());
                        interface->setPort1(port2);
                        Port* tempPort = port1;
                        port1 = port2;
                        port2 = tempPort;
                    }
                    interface->setPos2(port2->scenePos());
                    interface->setPort2(port2);
                    interface->updatePath();
                    interface = 0;
                    return true;
                }
            }
            else if (item && item->type() == Interface::Type)// && conn->getModifyPath())           // executed when path is modified and when connection is left in open
            {
                if (!interface->getModifyPath())
                {
                    delete interface;
                }
                //conn->setModifyPath(false);
                interface = 0;
                return true;
            }
            delete interface;
            interface = 0;
            return true;
        }
        break;
    }
    }
    return QObject::eventFilter(o, e);
}

void NodesEditor::save(QDataStream &ds)
{
    foreach(QGraphicsItem *item, scene->items())
        if (item->type() == SoftwareComponent::Type)
        {
            ds << item->type();
            ((SoftwareComponent*) item)->save(ds);
        }

    foreach(QGraphicsItem *item, scene->items())
        if (item->type() == Interface::Type)
        {
            ds << item->type();
            ((Interface*) item)->save(ds);
        }
}

void NodesEditor::load(QDataStream &ds)
{
    scene->clear();

    QMap<quint64, Port*> portMap;

    while (!ds.atEnd())
    {
        int type;
        ds >> type;
        if (type == SoftwareComponent::Type)
        {
            SoftwareComponent *block = new SoftwareComponent(0);
            scene->addItem(block);
            block->load(ds, portMap);
        } else if (type == Interface::Type)
        {
            Interface *conn = new Interface(0);
            scene->addItem(conn);
            conn->load(ds, portMap);
        }
    }
}
