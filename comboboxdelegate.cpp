/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "comboboxdelegate.h"

#include <QComboBox>
#include <QDebug>

ComboBoxDelegate::ComboBoxDelegate(QObject *parent, QVector <int> *indices, QVector < QStringList > *strings) :
    QItemDelegate(parent)
{
    if (indices != NULL && strings != NULL)
    {
        for (int iter = 0; iter < indices->size(); iter++)
        {
            columnIndices.push_back(indices->at(iter));
        }
        for (int iterX = 0; iterX < strings->size(); iterX++)
        {
            columnStrings.push_back(strings->at(iterX));
        }
    }
}

// TableView need to create an Editor
// Create Editor when we construct ComboBoxDelegate
// and return the Editor
QWidget* ComboBoxDelegate::createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
{
    if (columnIndices.size() != 0)
    {
        QComboBox *editor = new QComboBox(parent);
        for (int colIter = 0; colIter < columnIndices.size(); colIter++)
        {
            if (index.column() == columnIndices[colIter])
            {
                editor->addItems(columnStrings[colIter]);
                return editor;
            }
        }
    }
    return QItemDelegate::createEditor(parent, option, index);
}

// Then, we set the Editor
// Gets the data from Model and feeds the data to Editor
void ComboBoxDelegate::setEditorData(QWidget *editor,
                               const QModelIndex &index) const
{
    if (columnIndices.size() != 0)
    {
        for (int colIter = 0; colIter < columnIndices.size(); colIter++)
        {
            if (index.column() == columnIndices[colIter])
            {
                // Get the value via index of the Model
                QString str = index.model()->data(index, Qt::EditRole).toString();

                // Put the value into the QComboBox
                QComboBox *comboBox = static_cast<QComboBox*>(editor);
                for (int iter = 0; iter < columnStrings[colIter].size(); iter++)
                {
                    if (str == columnStrings[colIter][iter])
                    {
                        comboBox->setCurrentIndex(iter);
                        break;
                    }
                }
                return;
            }
        }
    }
    QItemDelegate::setEditorData(editor, index);
}

// When we modify data, this model reflect the change
// Data from the delegate to the model
void ComboBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if (columnIndices.size() != 0)
    {
        for (int colIter = 0; colIter < columnIndices.size(); colIter++)
        {
            if (index.column() == columnIndices[colIter])
            {
                QComboBox *comboBox = static_cast<QComboBox*>(editor);
                QString str = comboBox->currentText();
                model->setData(index, str, Qt::EditRole);
                return;
            }
        }
    }
    QItemDelegate::setModelData(editor, model, index);
}

// Give the QComboBox the info on size and location
void ComboBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (columnIndices.size() != 0)
    {
        for (int colIter = 0; colIter < columnIndices.size(); colIter++)
        {
            if (index.column() == columnIndices[colIter])
            {
                editor->setGeometry(option.rect);
                return;
            }
        }
    }
    QItemDelegate::updateEditorGeometry(editor, option, index);
}
