/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef INTERNALBEHAVIOURDIALOG_H
#define INTERNALBEHAVIOURDIALOG_H

#include <QDialog>
#include <QListWidgetItem>
#include "behaviourmodel.h"
#include "swcomponentmodel.h"
#include "interfacemodel.h"

namespace Ui {
class InternalBehaviourDialog;
}

class InternalBehaviourDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InternalBehaviourDialog(QWidget *parent = 0);
    ~InternalBehaviourDialog();

    void setData (QVector <InterfaceModel> *interfaces, QVector <SWComponentModel> *components, QVector <behaviourModel> *behaviours);
    behaviourModel getBehaviour();
    void editBehaviour(behaviourModel* behav);
    void done(int);

private slots:
    void on_addAttribute_clicked();
    void on_addRunnable_clicked();
    void on_runnablesList_currentRowChanged(int currentRow);
    void on_eventType_currentIndexChanged(int index);
    void on_eventPort_currentTextChanged(const QString &arg1);
    void on_eventOperation_currentTextChanged(const QString &arg1);
    void on_eventPeriod_editingFinished();
    void on_eventRTEEvent_editingFinished();
    void on_eventPort_currentIndexChanged(int index);
    void on_editAttribute_clicked();
    void on_removeAttribute_clicked();
    void enableAttributeTable();
    void populateEventPorts();
    void populateEventOperations();
    bool hasWhiteSpace(QString s);
    void on_removeRunnable_clicked();

private:
    Ui::InternalBehaviourDialog *ui;
    QVector <SWComponentModel> *components;
    QVector <InterfaceModel> *interfaces;
    QVector <behaviourModel> *behaviours;

    QVector <QString> runnableNames;
    QVector <float> minimumStartIntervals;
    QVector < QVector <QString> > attributeNames;
    QVector < QVector <QString> > attributeSWCPorts;
    QVector < QVector <QString> > attributeElements;

    QVector < QString > eventTypes;
    QVector < QString > eventColumn1;
    QVector < QString > eventColumn2;

    QString editBehaviourName;
};

#endif // INTERNALBEHAVIOURDIALOG_H
