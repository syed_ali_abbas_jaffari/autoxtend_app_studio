
**NOTE: To get an introduction of the AUTO-XTEND toolchain, please refer to the [main repository](https://bitbucket.org/syed_ali_abbas_jaffari/autoxtend).**

# Introduction to AUTOSAR App Studio

The tool AUTOSAR App Studio is used to define automotive system configuration, including application SWCs, their behaviours (with runnables) and system/timing constraints. Using this tool, one can generate an extract for an ECU in the system as an AUTOSAR XML (ARXML) file. The tool AUTOSAR App Studio generates an ECU extract compliant to the AUTOSAR standard 4.2, which enables direct import of this file in any commercial AUTOSAR tool, in turn, supporting portability and modularity. It is a GUI tool which complements system development using ARText (i.e., AUTOSAR textual language modeling framework). If required, the tool can directly import SoftWare Component Description (SWCD) files written in ARText languages.

# Getting Started

Note: Not tested on Windows. There might be problems due to different path convensions. In future, we intend to fix these issues.

## Compile & Run
Install Qt Creator on the desired operating system. Open project file in Qt Creator. If the compiler is detected by the Qt Creator, 'Configure Project' tab will appear. Press the 'Configure Project' button. This will parse and build compile hierarchy. Compile using Run button.

## Using GUI
Open files (one by one, types and interfaces first) using File->Open SWC files. If the error message related to the interface ResultIf appears, remove and recreate new line character in the file (TODO: Check why is it a problem and how to resolve it). Extract imports are not allowed. Once all the components are imported, create an instance of the software component for the extract by seleting the component from the software component list and clicking on the gear button below. Type the instance name and press OK. This will create an instance on the canvas. Add more components and connect them together by connecting the port boxes (Drag/Drop). When finished, goto File->Generate ECU Extract. This will create an extract ARXML file.

# Known Standard Limitations

1. Only one composition.
2. Supported port types: sender/receiver (no service ports), client/server (no service ports).
3. Supported component types: application.
4. Synchronous call of ClientServerOperation: Primitive types. No support for inout.
5. Supports only default types (e.g. uint8, uint32, etc.). No support for enumerations.
6. No support for application errors in interfaces.
7. Client/server ports only support 1 operation (don't know if more are possible).
8. Asynchrounous serverCallPoint not supported.
9. Supported events: operationInvokedEvent, timingEvent.
10. Symbols for runnables not supported.
11. Only C implementations are supported.

For a list of todos and known bugs, please see todo.txt.

# Wish to contribute?

You are welcome to contribute in improving and extending the software. To do that, you can request for a branch or report bugs (with procedure to reproduce the bug) to the author, Ali Syed @ [syed.ali.abbas.jaffari@gmail.com](mailto:syed.ali.abbas.jaffari@gmail.com). 

# Copyright and License

Copyright (C) 2016, Ali Syed, Germany
Contact: [syed.ali.abbas.jaffari@gmail.com](mailto:syed.ali.abbas.jaffari@gmail.com)

AUTO-XTEND is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. AUTO-XTEND is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU General Public License](https://www.gnu.org/licenses/gpl.txt) for more details.

**Disclaimer:** The rights for the operating system core from ArcCore and AUTOSAR meta-model provided as part of this toolchain are reserved by ArcCore AB, Sweden. The licensing statement from ArcCore is provided as under:

Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
Contact: [contact@arccore.com](mailto:contact@arccore.com)
You may ONLY use this file:

1. if you have a valid commercial ArcCore license and then in accordance with the terms contained in the written license agreement between you and ArcCore, or alternatively
2. if you follow the terms found in GNU General Public License version 2 as published by the Free Software Foundation and appearing in the file LICENSE.GPL included in the packaging of this file or [here](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt).

