/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "port.h"
#include "softwarecomponent.h"
#include "interface.h"

#include <QGraphicsTextItem>

Port::Port(QGraphicsItem *parent):
    QGraphicsPixmapItem(parent)
{
    label = new QGraphicsTextItem(this);

    setFlag(QGraphicsItem::ItemSendsScenePositionChanges);
}

Port::~Port()
{
    foreach(Interface *conn, m_connections)
        delete conn;
}

void Port::setNEBlock(SoftwareComponent *b)
{
    m_block = b;
}

void Port::setName(const QString &n)
{
    name = n;
    label->setPlainText(n);
}

void Port::setType(int type, bool isRport, bool isService)
{
    portType = type;
    isServicePort_ = isService;
    isRPort_ = isRport;
    if (!isRport)
        label->setPos(-27 - label->boundingRect().width(), -label->boundingRect().height()/2);
    else
        label->setPos(27, -label->boundingRect().height()/2);
    QPixmap img;
    if (type == 0)
    {
        img = QPixmap(":/ports/p-sr-no");
    }
    else if (type == 1 && isRport)
    {
        img = QPixmap(":/ports/p-cs-no");
    }
    else if (type == 1 && !isRport)
    {
        img = QPixmap(":/ports/r-cs-no");
    }
    setOffset(-img.width()/2,-img.height()/2);
    setPixmap(img);
}

int Port::getPortType()
{
    return portType;
}

bool Port::isRPort()
{
    return isRPort_;
}

bool Port::isServicePort()
{
    return isServicePort_;
}

QVector<Interface*>& Port::getConnections()
{
    return m_connections;
}

SoftwareComponent* Port::getBlock() const
{
    return m_block;
}

quint64 Port::getPtr()
{
    return m_ptr;
}

void Port::setPtr(quint64 p)
{
    m_ptr = p;
}

bool Port::isConnected(Port *other)
{
    foreach(Interface *conn, m_connections)
        if (conn->port1() == other || conn->port2() == other)
            return true;

    return false;
}

QVariant Port::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemScenePositionHasChanged)
    {
        foreach(Interface *conn, m_connections)
        {
            conn->updatePosFromPorts();
            conn->updatePath();
        }
    }
    return value;
}
