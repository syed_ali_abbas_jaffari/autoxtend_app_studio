/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef ATTRIBUTEDIALOG_H
#define ATTRIBUTEDIALOG_H

#include <QDialog>
#include "interfacemodel.h"
#include "swcomponentmodel.h"

namespace Ui {
class AttributeDialog;
}

class AttributeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AttributeDialog(QWidget *parent = 0);
    ~AttributeDialog();
    void setData(QVector <InterfaceModel> *interfaces, SWComponentModel *component);
    void getData(QString *attributeName, QString *portName, QString *elementName);
    void editAttribute(QString attributeName, QString portName, QString elementName);
    void done(int r);

private slots:
    void on_portName_currentIndexChanged(int index);
    void on_attribute_currentIndexChanged(int index);

private:
    Ui::AttributeDialog *ui;
    QVector <InterfaceModel> *interfaces;
    SWComponentModel *component;
};

#endif // ATTRIBUTEDIALOG_H
