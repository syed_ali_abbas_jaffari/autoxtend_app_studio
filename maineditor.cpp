/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "maineditor.h"
#include "ui_maineditor.h"
#include "createinterfacedialog.h"
#include "internalbehaviourdialog.h"
#include "createswcdialog.h"
#include "generateextractdialog.h"
#include "logger.h"
#include <QDebug>
#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include <QFormLayout>
#include <QLayout>
#include <uuid/uuid.h>
#include <QtXml>

MainEditor::MainEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainEditor)
{
    ui->setupUi(this);
    scene = new QGraphicsScene();

    QFile newLog("log.txt");
    newLog.remove();

    ui->graphicsView->setScene(scene);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing, true);
    setCentralWidget(ui->splitter);
    QList<int> sizes;
    sizes.push_back(this->size().width() * 2.5 / 3.5);
    sizes.push_back(this->size().width() * 1 / 3.5);
    ui->splitter->setSizes(sizes);

    nodesEditor = new NodesEditor(this);
    nodesEditor->install(scene);

    interfaceModels = new QVector<InterfaceModel>();
    componentModels = new QVector<SWComponentModel>();
    behaviourModels = new QVector<behaviourModel>();

    // The sequece for adding stuff should be the same
    /*SoftwareComponent *b = new SoftwareComponent(0);
    scene->addItem(b);
    b->setComponentType(0);
    b->setName("SWC1");
    b->setSubName("J");
    b->addPort("r-sr", 0, 0, true);
    b->addPort("r-cs", 0, 1, true);
    b->addPort("p-sr", 0, 0, false);
    b->addPort("p-cs", 0, 1, false);
    b->drawComponent();

    b = b->clone();
    b->setName("SWC2");
    b->setComponentType(1);
    b->drawComponent();
    b->setPos(250, 0);

    b = b->clone();
    b->setName("SWC3");
    b->setComponentType(2);
    b->drawComponent();
    b->setPos(250, 250);*/

}

MainEditor::~MainEditor()
{
    interfaceModels->clear();
    delete interfaceModels;
    componentModels->clear();
    delete componentModels;
    behaviourModels->clear();
    delete behaviourModels;
    delete ui;
}

void MainEditor::on_action_Quit_triggered()
{
    this->close();
}

void MainEditor::on_addInterfaceButton_clicked()
{
    CreateInterfaceDialog newDialog;
    newDialog.setModal(true);
    newDialog.setData(interfaceModels, componentModels, behaviourModels);
    if (newDialog.exec() == QDialog::Accepted)
    {
        InterfaceModel newInterface = newDialog.getInterface();
        interfaceModels->push_back(newInterface);
        ui->interfacesList->addItem(newInterface.interfaceName);
    }
}

void MainEditor::on_addSWCButton_clicked()
{
    if (interfaceModels->size() > 0)
    {
        CreateSWCDialog newDialog;
        newDialog.setModal(true);
        newDialog.setData(interfaceModels, componentModels, behaviourModels);
        if (newDialog.exec() == QDialog::Accepted)
        {
            SWComponentModel newComponent = newDialog.getComponent();
            componentModels->push_back(newComponent);
            ui->componentsList->addItem(newComponent.componentName);
        }
    }
    else
    {
        QMessageBox::information(this, "Error", "Please add an interface before creating a software component.");
    }
}

void MainEditor::on_actionOpen_ECU_Extract_triggered()
{
    QMessageBox::information(this, "Error", "This method is not yet implemented. Sorry for the inconvinience.");
}

void MainEditor::on_actionGenerate_SWC_files_triggered()
{
    QMessageBox::information(this, "Error", "This method is not yet implemented. Sorry for the inconvinience.");
    /*QString fname = QFileDialog::getSaveFileName();
    if (fname.isEmpty())
        return;

    QFile f(fname);
    f.open(QFile::WriteOnly);
    QDataStream ds(&f);
    nodesEditor->save(ds);*/
}

void MainEditor::on_actionOpen_SWC_files_triggered()
{
    QString fname = QFileDialog::getOpenFileName(this, tr("Open SWC File"),QString(),tr("SWC Files (*.swcd)"));
    if (fname.isEmpty())
        return;

    QFile f(fname);
    f.open(QFile::ReadOnly | QFile::Text);
    QTextStream in(&f);

    while(!in.atEnd()) {
        QString line = in.readLine();
        //qDebug() << "Line: " << line;
        QStringList fields = line.split(" ");
        if (fields[0].trimmed() == "interface")
        {
            InterfaceModel newInt;
            newInt.interfaceName = fields[2];
            newInt.interfaceType = fields[1];
            if (fields[1].trimmed() == "clientServer")
            {
                line = in.readLine();
                //qDebug() << "\tLine: " << line;
                fields = line.split(" ");
                if (fields[0].trimmed() != "operation")
                {
                    QMessageBox::information(this, "Error", "Unknown identifier " + fields[0].trimmed());
                    return;
                }
                newInt.operationName = fields[1].trimmed();
            }
            else if (fields[1].trimmed() == "senderReceiver")
            {
                newInt.operationName = "";
            }
            else
            {
                QMessageBox::information(this, "Error", "Unknown interface type " + fields[1].trimmed());
                return;
            }
            while (1)
            {
                line = in.readLine();
                //qDebug() << "\t\tLine: " << line;
                fields = line.split(" ");
                if (line.contains("}"))
                    break;
                newInt.dataNames.push_back(fields[2].trimmed());
                if (fields[1].trimmed() != "boolean" &&
                        fields[1].trimmed() != "uint8" &&
                        fields[1].trimmed() != "uint16" &&
                        fields[1].trimmed() != "uint32" &&
                        fields[1].trimmed() != "sint8" &&
                        fields[1].trimmed() != "sint16" &&
                        fields[1].trimmed() != "sint32" &&
                        fields[1].trimmed() != "float32" &&
                        fields[1].trimmed() != "float64" &&
                        fields[1].trimmed() != "void")
                {
                    QMessageBox::information(this, "Error", "Data type \'" + fields[1].trimmed() + "\' not supported. Supported data types: boolean, uint8, uint16, uint32, sint8, sint16, sint32, float32, float64, void.");
                    return;
                }
                newInt.dataType.push_back(fields[1].trimmed());
                if (newInt.operationName != "" && fields[0].trimmed() != "in" && fields[0].trimmed() != "out")
                {
                    QMessageBox::information(this, "Error", "Data direction \'" + fields[0].trimmed() + "\' not supported. Supported data directions: in, out.");
                    return;
                }
                newInt.dataDirection.push_back(fields[0].trimmed());
                //int lastIndex = newInt.dataNames.size()-1;
                //qDebug() << "Data name: " << newInt.dataNames.at(lastIndex) << ", type: " << newInt.dataType.at(lastIndex) << ", direction: " << newInt.dataDirection.at(lastIndex);
            }
            if (newInt.dataNames.size() == 0)
            {
                QMessageBox::information(this, "Error", "Error reading interface \'" + newInt.interfaceName + "\'. Make sure that the end of line is properly terminated with \\r\\n");
                return;
            }
            interfaceModels->push_back(newInt);
            ui->interfacesList->addItem(newInt.interfaceName);
        }
        else if (fields[0].trimmed() == "component")
        {
            if (fields[1].trimmed() != "application")
            {
                QMessageBox::information(this, "Error", "Unknown component type " + fields[1].trimmed());
                return;
            }
            SWComponentModel newSWC;
            newSWC.interfaces = interfaceModels;
            newSWC.componentName = fields[2].trimmed();
            newSWC.componentType = fields[1].trimmed();
            while (1)
            {
                line = in.readLine();
                fields = line.split(" ");
                if (line.contains("}"))
                    break;
                if (fields[0].trimmed() != "ports" && fields[0].trimmed() != "ports{")
                {
                    newSWC.portInterfaceTypeIndex.push_back(-1);
                    for (int intIter = 0; intIter < interfaceModels->size(); intIter++)
                    {
                        if (interfaceModels->at(intIter).interfaceName == fields[3].trimmed())
                        {
                            newSWC.portInterfaceTypeIndex[newSWC.portInterfaceTypeIndex.size()-1] = intIter;
                            break;
                        }
                    }
                    if (newSWC.portInterfaceTypeIndex[newSWC.portInterfaceTypeIndex.size()-1] == -1)
                    {
                        QMessageBox::information(this, "Error", "Interface " + fields[3].trimmed() + " not found.");
                        return;
                    }
                    newSWC.portNames.push_back(fields[1].trimmed());
                    newSWC.portType.push_back(fields[0].trimmed());
                }
            }
            componentModels->push_back(newSWC);
            ui->componentsList->addItem(newSWC.componentName);
        }
        else if (fields[0].trimmed() == "internalBehavior")
        {
            behaviourModel newBehav;
            newBehav.interfaces = interfaceModels;
            newBehav.components = componentModels;
            newBehav.componentTypeIndex = -1;
            for (int compIter = 0; compIter < componentModels->size(); compIter++)
            {
                if (fields[3].trimmed() == componentModels->at(compIter).componentName)
                {
                    newBehav.componentTypeIndex = compIter;
                    break;
                }
            }
            if (newBehav.componentTypeIndex == -1)
            {
                QMessageBox::information(this, "Error", "Component " + fields[3].trimmed() + " not found.");
                return;
            }
            newBehav.behaviourName = fields[1].trimmed();
            qDebug() << "Behaviour: " << fields[1].trimmed() << fields[3].trimmed();
            line = in.readLine();
            fields = line.split(" ");
            while (fields[0].trimmed() == "runnable")
            {
                if (fields[1].contains("["))
                {
                    QStringList div = fields[1].split("[");
                    int index = div[1].indexOf("]");
                    div[1].remove(index, div[1].size()-index);
                    newBehav.runnableNames.push_back(div[0].trimmed());
                    newBehav.minimumStartIntervals.push_back(div[1].toFloat());
                }
                else
                {
                    newBehav.runnableNames.push_back(fields[1].trimmed());
                    fields[2].remove(0, 1);
                    int index = fields[2].indexOf("]");
                    fields[2].remove(index, fields[2].size()-index);
                    newBehav.minimumStartIntervals.push_back(fields[2].trimmed().toFloat());
                }
                qDebug() << "Runnable name:" << newBehav.runnableNames[newBehav.runnableNames.size()-1] << ", " << newBehav.minimumStartIntervals[newBehav.runnableNames.size()-1];
                QVector <QString> attName;
                QVector <int> attPort, attElement;
                while (1)
                {
                    line = in.readLine();
                    fields = line.split(" ");
                    if (line.contains("}"))
                    {
                        line = in.readLine();
                        fields = line.split(" ");
                        break;
                    }
                    if (fields[0].trimmed() != "symbol")
                    {
                        if (fields[0].trimmed() == "serverCallPoint")
                        {
                            if (fields[1].trimmed() == "synchronous")
                            {
                                fields.removeAt(1);
                            }
                        }
                        if (fields[0].trimmed() == "dataReadAccess" || fields[0].trimmed() == "dataWriteAccess" || fields[0].trimmed() == "serverCallPoint")
                        {
                            qDebug() << "Attribute: " << fields;
                            attName.push_back(fields[0].trimmed());
                            attPort.push_back(-2);
                            attElement.push_back(-2);
                            QStringList p = fields[1].split(".");
                            for (int portIter = 0; portIter < componentModels->at(newBehav.componentTypeIndex).portNames.size(); portIter++)
                            {
                                if (componentModels->at(newBehav.componentTypeIndex).portNames[portIter] == p[0].trimmed())
                                {
                                    qDebug() << "Port" << p[0].trimmed() << "found @" << portIter;
                                    attPort[attPort.size()-1] = portIter;
                                    // Check that the type of ports are correct
                                    // TODO
                                    if (p[1] == "*")
                                    {
                                        attElement[attPort.size()-1] = -1;
                                    }
                                    else
                                    {
                                        if (fields[0].trimmed() == "serverCallPoint")
                                        {
                                            if (p[1].trimmed() != interfaceModels->at(componentModels->at(newBehav.componentTypeIndex).portInterfaceTypeIndex[portIter]).operationName)
                                            {
                                                QMessageBox::information(this, "Error", "Invalid operation name.");
                                                return;
                                            }
                                            attElement[attPort.size()-1] = -1;       // Since only one operation possible
                                        }
                                        else
                                        {
                                            bool found = false;
                                            for (int attIter = 0; attIter < interfaceModels->at(componentModels->at(newBehav.componentTypeIndex).portInterfaceTypeIndex[portIter]).dataNames.size(); attIter++)
                                            {
                                                if (p[1].trimmed() == interfaceModels->at(componentModels->at(newBehav.componentTypeIndex).portInterfaceTypeIndex[portIter]).dataNames[attIter])
                                                {
                                                    attElement[attPort.size()-1] = attIter;
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (!found)
                                            {
                                                QMessageBox::information(this, "Error", "Invalid data element name.");
                                                return;
                                            }
                                        }
                                        qDebug() << "Element:" << attElement[attPort.size()-1];
                                    }
                                    break;
                                }
                            }
                            if (attPort[attPort.size()-1] == -2 || attElement[attElement.size()-1] == -2)
                            {
                                QMessageBox::information(this, "Error", "Invalid port and/or element.");
                                return;
                            }
                        }
                        else if (fields[0].trimmed() == "timingEvent")
                        {
                            qDebug() << "TimingEvent:" << fields[1].trimmed();
                            newBehav.eventType.push_back(fields[0].trimmed());
                            newBehav.tEventPeriod.push_back(fields[1].trimmed().toFloat());
                            if (fields.size() > 2)
                            {
                                newBehav.tEventRTEEvent.push_back(fields[3].trimmed());
                            }
                            else
                            {
                                newBehav.tEventRTEEvent.push_back("");
                            }
                            newBehav.iEventPortIndex.push_back(0);
                        }
                        else if (fields[0].trimmed() == "operationInvokedEvent")
                        {
                            qDebug() << "OperationInvokedEvent:" << fields[1].trimmed();
                            newBehav.eventType.push_back(fields[0].trimmed());
                            newBehav.tEventPeriod.push_back(0.0f);
                            newBehav.tEventRTEEvent.push_back("");
                            QStringList p = fields[1].split(".");
                            for (int portIter = 0; portIter < componentModels->at(newBehav.componentTypeIndex).portNames.size(); portIter++)
                            {
                                if (componentModels->at(newBehav.componentTypeIndex).portNames[portIter] == p[0].trimmed())
                                {
                                    if (p[1].trimmed() != interfaceModels->at(componentModels->at(newBehav.componentTypeIndex).portInterfaceTypeIndex[portIter]).operationName)
                                    {
                                        QMessageBox::information(this, "Error", "Invalid operation name for event.");
                                        return;
                                    }
                                    newBehav.iEventPortIndex.push_back(portIter);
                                    break;
                                }
                            }
                        }
                    }
                }
                qDebug() << "pushing all attributes";
                newBehav.attributeNames.push_back(attName);
                newBehav.attributePortIndex.push_back(attPort);
                newBehav.attributeElementIndex.push_back(attElement);
                if (newBehav.eventType.size() != newBehav.attributeNames.size())
                {
                    newBehav.eventType.push_back("");
                    newBehav.tEventPeriod.push_back(0.0f);
                    newBehav.tEventRTEEvent.push_back("");
                    newBehav.iEventPortIndex.push_back(0);
                }
            }
            behaviourModels->push_back(newBehav);
            ui->behavioursList->addItem(newBehav.behaviourName);
        }
        else if (fields[0].trimmed() == "composition")
        {
            QMessageBox::information(this, "Error", "Compositions are not yet supported. Sorry for the inconvinience.");
            break;
        }
        else if (fields[0].trimmed() == "package" ||
                 fields[0].trimmed() == "import" ||
                 fields[0].trimmed() == "" ||
                 fields[0].trimmed() == "{" ||
                 fields[0].trimmed() == "{{" ||
                 fields[0].trimmed() == "}}" ||
                 fields[0].trimmed() == "}" ||
                 fields[0].trimmed() == "implementation" ||
                 fields[0].trimmed() == "language" ||
                 fields[0].trimmed() == "codeDescriptor" ||
                 fields[0].trimmed() == "@EcuExtract" ||
                 fields[0].trimmed() == "@ImplMapping")
        {
            // Ignore fields
        }
        else if (fields.size() > 1 && fields[1].trimmed() == "impl")
        {
            QMessageBox::information(this, "Error", "Implementations are not yet supported. Sorry for the inconvinience.");
            break;
        }
        else
        {
            QMessageBox::information(this, "Error", "Unknown identifier " + fields[0].trimmed());
            break;
        }
    }
    f.close();
}

void MainEditor::on_editInterfaceButton_clicked()
{
    if (ui->interfacesList->currentRow() != -1)
    {
        Log("Editting interface " + ui->interfacesList->item(ui->interfacesList->currentRow())->text());
        int editedIndex = ui->interfacesList->currentRow();
        CreateInterfaceDialog newDialog;
        newDialog.setModal(true);
        newDialog.setData(interfaceModels, componentModels, behaviourModels);
        newDialog.editInterface(interfaceModels->at(editedIndex));
        if (newDialog.exec() == QDialog::Accepted)
        {
            InterfaceModel edittedInterface = newDialog.getInterface();
            interfaceModels->erase(interfaceModels->begin() + editedIndex);
            interfaceModels->insert(interfaceModels->begin() + editedIndex, edittedInterface);
            ui->interfacesList->takeItem(editedIndex);
            ui->interfacesList->insertItem(editedIndex, edittedInterface.interfaceName);
        }
    }
}

void MainEditor::on_editSWCButton_clicked()
{
    if (ui->componentsList->currentRow() != -1)
    {
        Log("Editting component " + ui->componentsList->item(ui->componentsList->currentRow())->text());
        int editedIndex = ui->componentsList->currentRow();
        CreateSWCDialog newDialog;
        newDialog.setModal(true);
        newDialog.setData(interfaceModels, componentModels, behaviourModels);
        newDialog.editComponent(componentModels->at(editedIndex));
        if (newDialog.exec() == QDialog::Accepted)
        {
            SWComponentModel newComponent = newDialog.getComponent();
            componentModels->erase(componentModels->begin() + editedIndex);
            componentModels->insert(componentModels->begin() + editedIndex, newComponent);
            ui->componentsList->takeItem(editedIndex);
            ui->componentsList->insertItem(editedIndex, newComponent.componentName);
        }
    }
}

void MainEditor::on_removeInterfaceButton_clicked()
{
    if (ui->interfacesList->currentRow() != -1)
    {
        Log("Removing interface " + ui->interfacesList->item(ui->interfacesList->currentRow())->text());
        int editedIndex = ui->interfacesList->currentRow();
        // Check if some component is linked to this interface
        bool deleteInterface = false;
        QVector <int> linkedWithComponent;
        QVector <int> behaviourIndex;
        for (int compIter = 0; compIter < componentModels->size(); compIter++)
        {
            for (int portIter = 0; portIter < componentModels->at(compIter).portInterfaceTypeIndex.size(); portIter++)
            {
                if ((*componentModels)[compIter].getInterfaceType(portIter) == interfaceModels->at(editedIndex).interfaceName)
                {
                    linkedWithComponent.push_back(compIter);
                    // Check if the component has an implementation
                    qDebug() << "linkedWithComponent.add" << compIter;
                    behaviourIndex.push_back(-1);
                    /*for (int behIter = 0; behIter < behaviourModels->size(); behIter++)
                    {
                        if ((*behaviourModels)[behIter].getComponentType() == componentModels->at(compIter).componentName)
                        {
                            behaviourIndex[linkedWithComponent.size()-1] = behIter;
                            break;
                        }
                    }*/
                }
            }
        }

        // Ask if the linked component and/or behaviour should be adjusted
        QMessageBox::StandardButton reply;
        QVector < QVector <int> > deletedPortIndices;
        for (int compIter = 0; compIter < linkedWithComponent.size(); compIter++)
        {
            QVector <int> deletePIndPComp;
            if (behaviourIndex[compIter] == -1)
            {
                reply = QMessageBox::question(this, "Conflict", "Interface \'" + interfaceModels->at(editedIndex).interfaceName +
                                              "\' is required by the component \'" + componentModels->at(linkedWithComponent[compIter]).componentName +
                                              "\'. Before the removal of this interface, it must be removed from the component. Are you sure you want to proceed?", QMessageBox::Yes|QMessageBox::No);
            }
            else
            {
                reply = QMessageBox::question(this, "Conflict", "Interface \'" + interfaceModels->at(editedIndex).interfaceName +
                                              "\' is required by the component \'" + componentModels->at(linkedWithComponent[compIter]).componentName +
                                              "\' and by internal behaviour \'" + behaviourModels->at(behaviourIndex[compIter]).behaviourName +
                                              "\'. Before the removal of this interface, it must be removed from the component and the behaviour. Are you sure you want to proceed?", QMessageBox::Yes|QMessageBox::No);
            }
            if (reply == QMessageBox::Yes)
            {
                // Yes: Defer the deletion of the interface but eliminate the ports with this interface from the component
                deleteInterface = true;
                if (behaviourIndex[compIter] != -1)
                {
                    /*int portIndex = -1;
                    for (int portIter = 0; portIter < componentModels->at(compIter).portInterfaceTypeIndex.size(); portIter++)
                    {
                        if ((*componentModels)[compIter].getInterfaceType(portIter) == interfaceModels->at(editedIndex).interfaceName)
                        {
                            portIndex = portIter;
                        }
                    }
                    for (int runIter = 0; runIter < behaviourModentels->at(behaviourIndex[compIter]).runnableNames.size(); runIter++)
                    {
                        for (int attIter = 0; attIter < behaviourModels->at(behaviourIndex[compIter]).attributeNames.at(runIter).size(); attIter++)
                        {
                            if (componentModels->at(linkedWithComponent[compIter]).portNames.at(portIndex) == (*behaviourModels)[behaviourIndex[compIter]].getAttributePort(runIter, attIter))
                            {
                                (*behaviourModels)[behaviourIndex[compIter]].removeAttribute(runIter, attIter);
                            }
                        }
                        // Check operationInvokedEvent
                        if ((*behaviourModels)[behaviourIndex[compIter]].eventType[runIter] == "operationInvokedEvent")
                        {
                            (*behaviourModels)[behaviourIndex[compIter]].eventType[runIter] == "None";
                        }
                    }*/
                }
                for (int pIter = 0; pIter < componentModels->at(linkedWithComponent[compIter]).portNames.size(); pIter++)
                {
                    if ((*componentModels)[linkedWithComponent[compIter]].getInterfaceType(pIter) == interfaceModels->at(editedIndex).interfaceName)
                    {
                        qDebug() << "Deleting port" << (*componentModels)[linkedWithComponent[compIter]].portNames[pIter] <<
                                    "with interface" << (*componentModels)[linkedWithComponent[compIter]].getInterfaceType(pIter) <<
                                    "from component" << (*componentModels)[linkedWithComponent[compIter]].componentName;
                        (*componentModels)[linkedWithComponent[compIter]].removePort(pIter);
                        qDebug() << "deletedpIndPComp.add" << pIter;
                        deletePIndPComp.push_back(pIter);
                        pIter--;
                    }
                }
                deletedPortIndices.push_back(deletePIndPComp);
                qDebug() << "deletedPortIndeces.added.";
            }
            else
            {
                compIter = linkedWithComponent.size()-1;
                deleteInterface = false;
            }
        }
        if (linkedWithComponent.size() > 0)
        {
            if (deleteInterface == true)
            {
                interfaceModels->remove(editedIndex);
                ui->interfacesList->takeItem(editedIndex);
            }
            for (int compIter = 0; compIter < linkedWithComponent.size(); compIter++)
            {
                // update portInterfaceTypeIndices in componentmodel
                for (int indIter = 0; indIter < deletedPortIndices[compIter].size(); indIter++)
                {
                    for (int pIter2 = 0; pIter2 < componentModels->at(linkedWithComponent[compIter]).portNames.size(); pIter2++)
                    {
                        if ((*componentModels)[linkedWithComponent[compIter]].portInterfaceTypeIndex[pIter2] >= deletedPortIndices[compIter][indIter])
                        {
                            (*componentModels)[linkedWithComponent[compIter]].portInterfaceTypeIndex[pIter2]--;
                            qDebug() << "Component " << (*componentModels)[linkedWithComponent[compIter]].componentName << ":Port index" << pIter2 << "decremented";
                        }
                    }
                }
                // Clean up the empty components and their interfaces
                if (componentModels->at(linkedWithComponent[compIter]).portNames.size() == 0)
                {
                    // Check if the component has an implementation
                    /*for (int behIter = 0; behIter < behaviourModels->size(); behIter++)
                    {
                        if ((*behaviourModels)[behIter].getComponentType() == componentModels->at(linkedWithComponent[compIter]).componentName)
                        {
                            behaviourModels->remove(behIter);
                            ui->behavioursList->takeItem(behIter);
                            break;
                        }
                    }*/
                    qDebug() << "Component" << componentModels->at(linkedWithComponent[compIter]).componentName << "removed";
                    componentModels->remove(linkedWithComponent[compIter]);
                    ui->componentsList->takeItem(linkedWithComponent[compIter]);

                    // update linkedWithComponent
                    for (int lIter = compIter+1; lIter < linkedWithComponent.size(); lIter++)
                    {
                        linkedWithComponent[lIter]--;
                        qDebug() << "linkedWithComponent[" << lIter << "] = " << linkedWithComponent[lIter];
                    }

                    // update behaviourIndex
                    /*if (behaviourIndex[compIter] != -1)
                    {
                        for (int bIter = 0; bIter < behaviourIndex.size(); bIter++)
                        {
                            if (behaviourIndex[bIter] >= behaviourIndex[compIter])
                            {
                                behaviourIndex[bIter]--;
                            }
                        }
                    }*/
                }
            }
        }
        if (linkedWithComponent.size() == 0)
        {
            QMessageBox::StandardButton reply2;
            reply2 = QMessageBox::question(this, "Confirm", "Are you sure you want to remove this interface?", QMessageBox::Yes|QMessageBox::No);
            if (reply2 == QMessageBox::Yes)
            {
                interfaceModels->remove(editedIndex);
                ui->interfacesList->takeItem(editedIndex);
            }
        }
    }
}

void MainEditor::on_addBehaviour_clicked()
{
    if (componentModels->size() > 0)
    {
        InternalBehaviourDialog newDialog;
        newDialog.setModal(true);
        newDialog.setData(interfaceModels, componentModels, behaviourModels);
        if (newDialog.exec() == QDialog::Accepted)
        {
            behaviourModel newBeh = newDialog.getBehaviour();
            behaviourModels->push_back(newBeh);
            ui->behavioursList->addItem(newBeh.behaviourName);
        }
    }
    else
    {
        QMessageBox::information(this, "Error", "Please add a software component before creating an internal behaviour.");
    }
}

void MainEditor::on_instantiateSWC_clicked()
{
    if (ui->componentsList->currentRow() != -1)
    {
        bool ok = false;
        QString instanceName = QInputDialog::getText(this, "Create an Instance", "Type the instance name:", QLineEdit::Normal, QString(), &ok);
        if (ok)
        {
            SoftwareComponent *newComp = new SoftwareComponent(0);
            scene->addItem(newComp);
            newComp->setComponentType(0);
            newComp->setName(instanceName);
            newComp->setSubName(componentModels->at(ui->componentsList->currentRow()).componentName);
            for (int portIter = 0; portIter < componentModels->at(ui->componentsList->currentRow()).portNames.size(); portIter++)
            {
                int portType;
                bool isRPort;
                if (componentModels->at(ui->componentsList->currentRow()).portType[portIter] == "sender")
                {
                    portType = 0;
                    isRPort = false;
                }
                else if (componentModels->at(ui->componentsList->currentRow()).portType[portIter] == "receiver")
                {
                    portType = 0;
                    isRPort = true;
                }
                else if (componentModels->at(ui->componentsList->currentRow()).portType[portIter] == "client")
                {
                    portType = 1;
                    isRPort = true;
                }
                else
                {
                    portType = 1;
                    isRPort = false;
                }
                newComp->addPort(componentModels->at(ui->componentsList->currentRow()).portNames[portIter], 0, portType, isRPort);
            }
            newComp->setPos(ui->graphicsView->sceneRect().center().toPoint());
            newComp->drawComponent();
        }
    }
}

void MainEditor::on_removeSWCButton_clicked()
{
    if (ui->componentsList->currentRow() != -1)
    {
        Log("Removing component " + ui->componentsList->item(ui->componentsList->currentRow())->text());
        int behaviourToRemove = -1;
        for (int behIter = 0; behIter < behaviourModels->size(); behIter++)
        {
            if ((*behaviourModels)[behIter].getComponentType() == componentModels->at(ui->componentsList->currentRow()).componentName)
            {
                behaviourToRemove = behIter;
                break;
            }
        }
        QMessageBox::StandardButton reply1;
        if (behaviourToRemove != -1)
        {
            reply1 = QMessageBox::question(this, "Confirm", "Removing this component may lead to removal of the behaviour \'" + behaviourModels->at(behaviourToRemove).behaviourName + "\'. Are you sure you want to proceed?", QMessageBox::Yes|QMessageBox::No);
        }
        else
        {
            reply1 = QMessageBox::question(this, "Confirm", "Are you sure you want to remove this component?", QMessageBox::Yes|QMessageBox::No);
        }
        if (reply1 == QMessageBox::Yes)
        {
            if (behaviourToRemove != -1)
            {
                behaviourModels->erase(behaviourModels->begin() + behaviourToRemove);
                ui->behavioursList->takeItem(behaviourToRemove);
            }
            componentModels->erase(componentModels->begin() + ui->componentsList->currentRow());
            ui->componentsList->takeItem(ui->componentsList->currentRow());
        }
    }
}

void MainEditor::on_removeBehaviour_clicked()
{
    if (ui->behavioursList->currentRow() != -1)
    {
        Log("Removing behaviour " + ui->behavioursList->item(ui->behavioursList->currentRow())->text());
        QMessageBox::StandardButton reply1;
        reply1 = QMessageBox::question(this, "Confirm", "Are you sure you want to remove this behaviour?", QMessageBox::Yes|QMessageBox::No);
        if (reply1 == QMessageBox::Yes)
        {
            behaviourModels->remove(ui->behavioursList->currentRow());
            ui->behavioursList->takeItem(ui->behavioursList->currentRow());
        }
    }
}

void MainEditor::on_editBehaviour_clicked()
{
    if (ui->behavioursList->currentRow() != -1)
    {
        Log("Editting behaviour " + ui->behavioursList->item(ui->behavioursList->currentRow())->text());
        int editedIndex = ui->behavioursList->currentRow();
        InternalBehaviourDialog newDialog;
        newDialog.setModal(true);
        newDialog.setData(interfaceModels, componentModels, behaviourModels);
        newDialog.editBehaviour(&(*behaviourModels)[editedIndex]);
        if (newDialog.exec() == QDialog::Accepted)
        {
            behaviourModel newBehaviour = newDialog.getBehaviour();
            behaviourModels->erase(behaviourModels->begin() + editedIndex);
            behaviourModels->insert(behaviourModels->begin() + editedIndex, newBehaviour);
            ui->behavioursList->takeItem(editedIndex);
            ui->behavioursList->insertItem(editedIndex, newBehaviour.behaviourName);
        }
    }
}

void MainEditor::on_actionGenerate_ECU_Extract_triggered()
{
    // Check if there exist a component in canvas area
    if (scene->items().size() == 0)
    {
        QMessageBox::information(this, "Error", "Unable to find any software component on the canvas.", QMessageBox::Ok);
        return;
    }

    GenerateExtractDialog dialog(this);

    // Show the dialog as modal
    if (dialog.exec() == QDialog::Accepted)
    {
        QStringList returnedData = dialog.getData();
        if (!returnedData[3].toUpper().contains(".XML") & !returnedData[3].toUpper().contains(".ARXML"))
        {
            returnedData[3] += ".arxml";
        }
        generateEcuExtract(returnedData[0], returnedData[1], returnedData[2], returnedData[4]+"/"+returnedData[3]);
    }
}

void MainEditor::generateEcuExtract(QString rootPackageName, QString compositionName, QString extractName, QString fileName)
{
    //https://techbase.kde.org/Development/Tutorials/QtDOM_Tutorial
    // Common elements
    QDomElement shortName;

    QDomDocument doc;
    addAttribute("xmlns", "http://autosar.org/schema/r4.0");
    addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
    addAttribute("xsi:schemaLocation", "http://autosar.org/schema/r4.0 autosar_4-0-3.xsd");
    QDomElement root = createSimpleElement(doc, "AUTOSAR", &attrName, &attrValue);
    clearAttributes();

    QDomElement mainPackages = doc.createElement("AR-PACKAGES");
    QDomElement rootPackage = doc.createElement("AR-PACKAGE");
    root.appendChild(mainPackages);
    mainPackages.appendChild(rootPackage);

    // Add name of the root package
    shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, rootPackageName);
    rootPackage.appendChild(shortName);
    shortName.clear();

    // Add elements
    QDomElement rootPackageElements = doc.createElement("ELEMENTS");
    rootPackage.appendChild(rootPackageElements);

    // Add composition
    QDomElement composition = doc.createElement("COMPOSITION-SW-COMPONENT-TYPE");
    composition.setAttribute("UUID", generateUUID());
    {
        // Add composition name
        shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, compositionName);
        composition.appendChild(shortName);
        shortName.clear();

        // Add components tag
        QDomElement compositionComponents = doc.createElement("COMPONENTS");
        composition.appendChild(compositionComponents);

        // Add software components
        foreach(QGraphicsItem *item, scene->items())
        {
            if (item->type() == SoftwareComponent::Type)
            {
                addAttribute("UUID", generateUUID());
                QDomElement componentPrototype = createSimpleElement(doc, "SW-COMPONENT-PROTOTYPE", &attrName, &attrValue);
                compositionComponents.appendChild(componentPrototype);
                clearAttributes();

                shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, ((SoftwareComponent*) item)->getName());
                componentPrototype.appendChild(shortName);
                shortName.clear();

                addAttribute("DEST", "APPLICATION-SW-COMPONENT-TYPE");
                QDomElement typeRef = createSimpleElement(doc, "TYPE-TREF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+((SoftwareComponent*) item)->getSubName());
                componentPrototype.appendChild(typeRef);
                clearAttributes();

            }
        }

        // Add connectors tag
        QDomElement compositionConnectors = doc.createElement("CONNECTORS");
        composition.appendChild(compositionConnectors);

        // Add software connectors
        foreach(QGraphicsItem *item, scene->items())
        {
            if (item->type() == Interface::Type)
            {
                addAttribute("UUID", generateUUID());
                QDomElement assemblyConnector = createSimpleElement(doc, "ASSEMBLY-SW-CONNECTOR", &attrName, &attrValue);
                clearAttributes();

                Interface *interface = ((Interface*) item);
                if (interface->port1()->isRPort())
                {
                    shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, interface->port2()->getBlock()->getName()+"_"+interface->port2()->getPortName()+"_to_"+interface->port1()->getBlock()->getName()+"_"+interface->port1()->getPortName());
                    assemblyConnector.appendChild(shortName);
                    shortName.clear();

                    QDomElement providerIRef = doc.createElement("PROVIDER-IREF");
                    assemblyConnector.appendChild(providerIRef);

                    addAttribute("DEST", "SW-COMPONENT-PROTOTYPE");
                    QDomElement componentRef = createSimpleElement(doc, "CONTEXT-COMPONENT-REF", &attrName, &attrValue, "/"+rootPackageName+"/"+compositionName+"/"+interface->port2()->getBlock()->getName());
                    clearAttributes();
                    providerIRef.appendChild(componentRef);

                    addAttribute("DEST", "P-PORT-PROTOTYPE");
                    QDomElement targetPort = createSimpleElement(doc, "TARGET-P-PORT-REF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+interface->port2()->getBlock()->getSubName()+"/"+interface->port2()->getPortName());
                    clearAttributes();
                    providerIRef.appendChild(targetPort);

                    QDomElement requesterIRef = doc.createElement("REQUESTER-IREF");
                    assemblyConnector.appendChild(requesterIRef);

                    addAttribute("DEST", "SW-COMPONENT-PROTOTYPE");
                    componentRef = createSimpleElement(doc, "CONTEXT-COMPONENT-REF", &attrName, &attrValue, "/"+rootPackageName+"/"+compositionName+"/"+interface->port1()->getBlock()->getName());
                    clearAttributes();
                    requesterIRef.appendChild(componentRef);

                    addAttribute("DEST", "R-PORT-PROTOTYPE");
                    targetPort = createSimpleElement(doc, "TARGET-R-PORT-REF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+interface->port1()->getBlock()->getSubName()+"/"+interface->port1()->getPortName());
                    clearAttributes();
                    requesterIRef.appendChild(targetPort);
                }
                else
                {
                    qDebug() << "Error: Port2 is R-port. Generated extract may not be correct";
                }
                compositionConnectors.appendChild(assemblyConnector);
            }
        }
    }
    rootPackageElements.appendChild(composition);

    QDomElement system = doc.createElement("SYSTEM");
    system.setAttribute("UUID", generateUUID());
    {
        // Add extract name
        shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, extractName);
        system.appendChild(shortName);
        shortName.clear();

        // Add category name
        shortName = createSimpleElement(doc, "CATEGORY", NULL, NULL, "ECU_EXTRACT");
        system.appendChild(shortName);
        shortName.clear();

        // Add mappings tag
        QDomElement mappings = doc.createElement("MAPPINGS");
        system.appendChild(mappings);

        {
            // Add system mapping
            addAttribute("UUID", generateUUID());
            QDomElement systemMapping = createSimpleElement(doc, "SYSTEM-MAPPING", &attrName, &attrValue);
            mappings.appendChild(systemMapping);
            clearAttributes();

            // Add Implementation Mappings short name
            shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, "ImplementationMappings");
            systemMapping.appendChild(shortName);
            shortName.clear();

            // Add Implementation Mappings
            QDomElement implementationMappings = doc.createElement("SW-IMPL-MAPPINGS");
            systemMapping.appendChild(implementationMappings);

            // Add swc to implementation mappings
            foreach(QGraphicsItem *item, scene->items())
            {
                if (item->type() == SoftwareComponent::Type)
                {
                    addAttribute("UUID", generateUUID());
                    QDomElement softwareToImplMapping = createSimpleElement(doc, "SWC-TO-IMPL-MAPPING", &attrName, &attrValue);
                    implementationMappings.appendChild(softwareToImplMapping);
                    clearAttributes();

                    shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, ((SoftwareComponent*) item)->getName()+"Mapping");
                    softwareToImplMapping.appendChild(shortName);
                    shortName.clear();

                    addAttribute("DEST", "SWC-IMPLEMENTATION");
                    QDomElement compRef = createSimpleElement(doc, "COMPONENT-IMPLEMENTATION-REF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+((SoftwareComponent*) item)->getSubName()+"Implementation");
                    softwareToImplMapping.appendChild(compRef);
                    clearAttributes();

                    QDomElement compIRefs = doc.createElement("COMPONENT-IREFS");
                    softwareToImplMapping.appendChild(compIRefs);

                    QDomElement compIRef = doc.createElement("COMPONENT-IREF");
                    compIRefs.appendChild(compIRef);

                    addAttribute("DEST", "ROOT-SW-COMPOSITION-PROTOTYPE");
                    QDomElement componentRef = createSimpleElement(doc, "CONTEXT-COMPOSITION-REF", &attrName, &attrValue, "/"+rootPackageName+"/"+extractName+"/"+compositionName+"Prototype");
                    clearAttributes();
                    compIRef.appendChild(componentRef);

                    addAttribute("DEST", "SW-COMPONENT-PROTOTYPE");
                    QDomElement targetPort = createSimpleElement(doc, "TARGET-COMPONENT-REF", &attrName, &attrValue, "/"+rootPackageName+"/"+compositionName+"/"+((SoftwareComponent*) item)->getName());
                    clearAttributes();
                    compIRef.appendChild(targetPort);
                }
            }
        }

        QDomElement rootComposition = doc.createElement("ROOT-SOFTWARE-COMPOSITIONS");
        system.appendChild(rootComposition);

        addAttribute("UUID", generateUUID());
        QDomElement rootCompositionPrototype = createSimpleElement(doc, "ROOT-SW-COMPOSITION-PROTOTYPE", &attrName, &attrValue);
        rootComposition.appendChild(rootCompositionPrototype);
        clearAttributes();

        // Add short name
        shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, compositionName + "Prototype");
        rootCompositionPrototype.appendChild(shortName);
        shortName.clear();

        // Add software composition
        addAttribute("DEST", "COMPOSITION-SW-COMPONENT-TYPE");
        QDomElement comp = createSimpleElement(doc, "SOFTWARE-COMPOSITION-TREF", &attrName, &attrValue, "/"+rootPackageName+"/"+compositionName);
        rootCompositionPrototype.appendChild(comp);
        clearAttributes();
    }
    rootPackageElements.appendChild(system);

    // Add sub-packages
    QDomElement subPackages = doc.createElement("AR-PACKAGES");
    rootPackage.appendChild(subPackages);
    QDomElement componentPackage = doc.createElement("AR-PACKAGE");
    subPackages.appendChild(componentPackage);
    QDomElement typesPackage = doc.createElement("AR-PACKAGE");
    subPackages.appendChild(typesPackage);

    // Add component package data
    shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, "Components");
    componentPackage.appendChild(shortName);
    shortName.clear();

    QDomElement componentElements = doc.createElement("ELEMENTS");
    componentPackage.appendChild(componentElements);

    // Add each component and its implementation
    foreach(QGraphicsItem *item, scene->items())
    {
        if (item->type() == SoftwareComponent::Type)
        {
            SoftwareComponent *component = ((SoftwareComponent*) item);

            addAttribute("UUID", generateUUID());
            QDomElement applicationSoftwareComponent = createSimpleElement(doc, "APPLICATION-SW-COMPONENT-TYPE", &attrName, &attrValue);
            componentElements.appendChild(applicationSoftwareComponent);
            clearAttributes();

            // Add short name
            shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, component->getSubName());
            applicationSoftwareComponent.appendChild(shortName);
            shortName.clear();

            // Look for the software component
            int compIndex = -1;
            for (int iter = 0; iter < componentModels->size(); iter++)
            {
                if (component->getSubName() == componentModels->at(iter).componentName)
                {
                    compIndex = iter;
                    break;
                }
            }
            if (compIndex == -1)
            {
                QMessageBox::information(this, "Error", "No software component exist with the name " + component->getName(), QMessageBox::Ok);
                return;
            }

            // Add ports
            QDomElement ports = doc.createElement("PORTS");
            applicationSoftwareComponent.appendChild(ports);
            for (int portIter = 0; portIter < componentModels->at(compIndex).portNames.size(); portIter++)
            {
                QDomElement portPrototype;
                addAttribute("UUID", generateUUID());
                if (componentModels->at(compIndex).portType[portIter] == "client" || componentModels->at(compIndex).portType[portIter] == "receiver")     // Checking for r-port
                {
                    portPrototype = createSimpleElement(doc, "R-PORT-PROTOTYPE", &attrName, &attrValue);
                }
                else        // port is p-port
                {
                    portPrototype = createSimpleElement(doc, "P-PORT-PROTOTYPE", &attrName, &attrValue);
                }
                ports.appendChild(portPrototype);
                clearAttributes();

                // Add short name
                shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, componentModels->at(compIndex).portNames[portIter]);
                portPrototype.appendChild(shortName);
                shortName.clear();


                QDomElement interfaceRef;
                if (componentModels->at(compIndex).portType[portIter] == "client")
                {
                    addAttribute("DEST", "CLIENT-SERVER-INTERFACE");
                    interfaceRef = createSimpleElement(doc, "REQUIRED-INTERFACE-TREF", &attrName, &attrValue, "/"+rootPackageName+"/Types/"+interfaceModels->at(componentModels->at(compIndex).portInterfaceTypeIndex[portIter]).interfaceName);
                    clearAttributes();
                }
                else if (componentModels->at(compIndex).portType[portIter] == "server")
                {
                    addAttribute("DEST", "CLIENT-SERVER-INTERFACE");
                    interfaceRef = createSimpleElement(doc, "PROVIDED-INTERFACE-TREF", &attrName, &attrValue, "/"+rootPackageName+"/Types/"+interfaceModels->at(componentModels->at(compIndex).portInterfaceTypeIndex[portIter]).interfaceName);
                    clearAttributes();
                }
                else if (componentModels->at(compIndex).portType[portIter] == "sender")
                {
                    addAttribute("DEST", "SENDER-RECEIVER-INTERFACE");
                    interfaceRef = createSimpleElement(doc, "PROVIDED-INTERFACE-TREF", &attrName, &attrValue, "/"+rootPackageName+"/Types/"+interfaceModels->at(componentModels->at(compIndex).portInterfaceTypeIndex[portIter]).interfaceName);
                    clearAttributes();
                }
                else if (componentModels->at(compIndex).portType[portIter] == "receiver")
                {
                    addAttribute("DEST", "SENDER-RECEIVER-INTERFACE");
                    interfaceRef = createSimpleElement(doc, "REQUIRED-INTERFACE-TREF", &attrName, &attrValue, "/"+rootPackageName+"/Types/"+interfaceModels->at(componentModels->at(compIndex).portInterfaceTypeIndex[portIter]).interfaceName);
                    clearAttributes();
                }
                portPrototype.appendChild(interfaceRef);
            }


            // Add internal behaviours
            QDomElement internalBehaviours = doc.createElement("INTERNAL-BEHAVIORS");
            applicationSoftwareComponent.appendChild(internalBehaviours);

            addAttribute("UUID", generateUUID());
            QDomElement swcInternalBehav = createSimpleElement(doc, "SWC-INTERNAL-BEHAVIOR", &attrName, &attrValue);
            internalBehaviours.appendChild(swcInternalBehav);
            clearAttributes();

            // Look for internal implementation
            int behavIndex = -1;
            for (int iter = 0; iter < behaviourModels->size(); iter++)
            {
                if (component->getSubName() == componentModels->at(behaviourModels->at(iter).componentTypeIndex).componentName)
                {
                    behavIndex = iter;
                    break;
                }
            }
            if (behavIndex == -1)
            {
                QMessageBox::information(this, "Error", "Software component " + component->getSubName() + " does not have an implementation.",  QMessageBox::Ok);
                return;
            }

            shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, behaviourModels->at(behavIndex).behaviourName);
            swcInternalBehav.appendChild(shortName);
            shortName.clear();

            QDomElement events = doc.createElement("EVENTS");
            swcInternalBehav.appendChild(events);

            // Add events
            for (int runIter = 0; runIter < behaviourModels->at(behavIndex).runnableNames.size(); runIter++)
            {
                if (behaviourModels->at(behavIndex).eventType.at(runIter) == "timingEvent")
                {
                    addAttribute("UUID", generateUUID());
                    QDomElement timEvent = createSimpleElement(doc, "TIMING-EVENT", &attrName, &attrValue);
                    events.appendChild(timEvent);
                    clearAttributes();

                    if (behaviourModels->at(behavIndex).tEventRTEEvent.at(runIter) != "")
                    {
                        shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, behaviourModels->at(behavIndex).tEventRTEEvent.at(runIter));
                        timEvent.appendChild(shortName);
                        shortName.clear();
                    }

                    addAttribute("DEST", "RUNNABLE-ENTITY");
                    QDomElement eventRef = createSimpleElement(doc, "START-ON-EVENT-REF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+component->getSubName()+"/"+behaviourModels->at(behavIndex).behaviourName+"/"+behaviourModels->at(behavIndex).runnableNames.at(runIter));
                    timEvent.appendChild(eventRef);
                    clearAttributes();

                    QDomElement period = createSimpleElement(doc, "PERIOD", NULL, NULL, QString::number(behaviourModels->at(behavIndex).tEventPeriod[runIter]));
                    timEvent.appendChild(period);
                }
                else if (behaviourModels->at(behavIndex).eventType.at(runIter) == "operationInvokedEvent")
                {
                    addAttribute("UUID", generateUUID());
                    QDomElement opEvent = createSimpleElement(doc, "OPERATION-INVOKED-EVENT", &attrName, &attrValue);
                    events.appendChild(opEvent);
                    clearAttributes();

                    QString refPortName = componentModels->at(compIndex).portNames.at(behaviourModels->at(behavIndex).iEventPortIndex.at(runIter));
                    QString refOpName = interfaceModels->at(componentModels->at(compIndex).portInterfaceTypeIndex.at(behaviourModels->at(behavIndex).iEventPortIndex.at(runIter))).operationName;
                    shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, refPortName + "_" + refOpName);
                    opEvent.appendChild(shortName);
                    shortName.clear();

                    addAttribute("DEST", "RUNNABLE-ENTITY");
                    QDomElement eventRef = createSimpleElement(doc, "START-ON-EVENT-REF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+component->getSubName()+"/"+behaviourModels->at(behavIndex).behaviourName+"/"+behaviourModels->at(behavIndex).runnableNames.at(runIter));
                    opEvent.appendChild(eventRef);
                    clearAttributes();

                    QDomElement operationIRef = doc.createElement("OPERATION-IREF");
                    opEvent.appendChild(operationIRef);

                    addAttribute("DEST", "P-PORT-PROTOTYPE");
                    QDomElement contextRef = createSimpleElement(doc, "CONTEXT-P-PORT-REF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+component->getSubName()+"/"+refPortName);
                    operationIRef.appendChild(contextRef);
                    clearAttributes();

                    addAttribute("DEST", "CLIENT-SERVER-OPERATION");
                    QDomElement targetOpRef = createSimpleElement(doc, "TARGET-PROVIDED-OPERATION-REF", &attrName, &attrValue, "/"+rootPackageName+"/Types/"+interfaceModels->at(componentModels->at(compIndex).portInterfaceTypeIndex.at(behaviourModels->at(behavIndex).iEventPortIndex.at(runIter))).interfaceName+"/"+refOpName);
                    operationIRef.appendChild(targetOpRef);
                    clearAttributes();
                }
            }

            QDomElement termRestart = createSimpleElement(doc, "HANDLE-TERMINATION-AND-RESTART", NULL, NULL, "NO-SUPPORT");
            swcInternalBehav.appendChild(termRestart);

            QDomElement runnables = doc.createElement("RUNNABLES");
            swcInternalBehav.appendChild(runnables);

            // Add runnables
            for (int runIter = 0; runIter < behaviourModels->at(behavIndex).runnableNames.size(); runIter++)
            {
                qDebug() << "Runnable " << behaviourModels->at(behavIndex).runnableNames.at(runIter);
                addAttribute("UUID", generateUUID());
                QDomElement runEntity = createSimpleElement(doc, "RUNNABLE-ENTITY", &attrName, &attrValue);
                runnables.appendChild(runEntity);
                clearAttributes();

                shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, behaviourModels->at(behavIndex).runnableNames.at(runIter));
                runEntity.appendChild(shortName);
                shortName.clear();

                QDomElement minInterval = createSimpleElement(doc, "MINIMUM-START-INTERVAL", NULL, NULL, QString::number(behaviourModels->at(behavIndex).minimumStartIntervals.at(runIter), 'f', 1));
                runEntity.appendChild(minInterval);

                QDomElement invokeConcurrently = createSimpleElement(doc, "CAN-BE-INVOKED-CONCURRENTLY", NULL, NULL, "false");
                runEntity.appendChild(invokeConcurrently);

                for (int attrIter = 0; attrIter < behaviourModels->at(behavIndex).attributeNames.at(runIter).size(); attrIter++)
                {
                    int interIndex = componentModels->at(compIndex).portInterfaceTypeIndex.at(behaviourModels->at(behavIndex).attributePortIndex.at(runIter).at(attrIter));
                    qDebug() << "\tAccesses interface " << interfaceModels->at(interIndex).interfaceName;
                    if (behaviourModels->at(behavIndex).attributeNames.at(runIter).at(attrIter) == "dataWriteAccess" || behaviourModels->at(behavIndex).attributeNames.at(runIter).at(attrIter) == "dataReadAccess")
                    {
                        QDomElement dataAcesses;
                        if (behaviourModels->at(behavIndex).attributeNames.at(runIter).at(attrIter) == "dataWriteAccess")
                            dataAcesses = doc.createElement("DATA-WRITE-ACCESSS");
                        else
                            dataAcesses = doc.createElement("DATA-READ-ACCESSS");
                        runEntity.appendChild(dataAcesses);

                        int iterStart = 0;
                        int iterEnd = interfaceModels->at(interIndex).dataNames.size();
                        if (iterEnd == 0)
                        {
                            QMessageBox::information(this, "Error", "Error in interface " + interfaceModels->at(interIndex).interfaceName + ": No data element exists.",  QMessageBox::Ok);
                            return;
                        }
                        if (behaviourModels->at(behavIndex).attributeElementIndex.at(runIter).at(attrIter) != -1)
                        {
                            iterStart = behaviourModels->at(behavIndex).attributeElementIndex.at(runIter).at(attrIter);
                            iterEnd = behaviourModels->at(behavIndex).attributeElementIndex.at(runIter).at(attrIter)+1;
                        }
                        qDebug() << "\tdataSize = " << interfaceModels->at(interIndex).dataNames.size() << ", data index = " << QString::number(behaviourModels->at(behavIndex).attributeElementIndex.at(runIter).at(attrIter)) << ", iterStart = " << iterStart << ", iterEnd = " << iterEnd;
                        for (int varIter = iterStart; varIter < iterEnd; varIter++)
                        {
                            addAttribute("UUID", generateUUID());
                            QDomElement variableAccesses = createSimpleElement(doc, "VARIABLE-ACCESS", &attrName, &attrValue);
                            dataAcesses.appendChild(variableAccesses);
                            clearAttributes();

                            if (behaviourModels->at(behavIndex).attributeNames.at(runIter).at(attrIter) == "dataWriteAccess")
                                shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, "dataWriteAccess_" +
                                                            behaviourModels->at(behavIndex).runnableNames.at(runIter) + "_" +
                                                            componentModels->at(compIndex).portNames.at(behaviourModels->at(behavIndex).attributePortIndex.at(runIter).at(attrIter)) + "_" +
                                                            interfaceModels->at(interIndex).dataNames.at(varIter));
                            else
                                shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, "dataReadAccess_" +
                                                        behaviourModels->at(behavIndex).runnableNames.at(runIter) + "_" +
                                                        componentModels->at(compIndex).portNames.at(behaviourModels->at(behavIndex).attributePortIndex.at(runIter).at(attrIter)) + "_" +
                                                        interfaceModels->at(interIndex).dataNames.at(varIter));
                            variableAccesses.appendChild(shortName);
                            shortName.clear();

                            QDomElement accessedVariable = doc.createElement("ACCESSED-VARIABLE");
                            variableAccesses.appendChild(accessedVariable);

                            QDomElement variableRef = doc.createElement("AUTOSAR-VARIABLE-IREF");
                            accessedVariable.appendChild(variableRef);

                            if (behaviourModels->at(behavIndex).attributeNames.at(runIter).at(attrIter) == "dataWriteAccess")
                                addAttribute("DEST", "P-PORT-PROTOTYPE");
                            else
                                addAttribute("DEST", "R-PORT-PROTOTYPE");
                            QDomElement portRef = createSimpleElement(doc, "PORT-PROTOTYPE-REF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+component->getSubName()+"/"+componentModels->at(compIndex).portNames.at(behaviourModels->at(behavIndex).attributePortIndex.at(runIter).at(attrIter)));
                            variableRef.appendChild(portRef);
                            clearAttributes();

                            addAttribute("DEST", "VARIABLE-DATA-PROTOTYPE");
                            QDomElement targetRef = createSimpleElement(doc, "TARGET-DATA-PROTOTYPE-REF", &attrName, &attrValue, "/"+rootPackageName+"/Types/"+
                                                                        interfaceModels->at(interIndex).interfaceName+"/"+
                                                                        interfaceModels->at(interIndex).dataNames.at(varIter));
                            variableRef.appendChild(targetRef);
                            clearAttributes();
                        }
                    }
                    else if (behaviourModels->at(behavIndex).attributeNames.at(runIter).at(attrIter) == "serverCallPoint")
                    {
                        QDomElement callPoints = doc.createElement("SERVER-CALL-POINTS");
                        runEntity.appendChild(callPoints);

                        addAttribute("UUID", generateUUID());
                        QDomElement callPoint = createSimpleElement(doc, "SYNCHRONOUS-SERVER-CALL-POINT", &attrName, &attrValue);
                        callPoints.appendChild(callPoint);
                        clearAttributes();

                        shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, "serverCallPoint_" + behaviourModels->at(behavIndex).runnableNames.at(runIter) + "_" + componentModels->at(compIndex).portNames.at(behaviourModels->at(behavIndex).attributePortIndex.at(runIter).at(attrIter)) + "_" + interfaceModels->at(interIndex).operationName);
                        callPoint.appendChild(shortName);
                        shortName.clear();

                        QDomElement opRef = doc.createElement("OPERATION-IREF");
                        callPoint.appendChild(opRef);

                        addAttribute("DEST", "R-PORT-PROTOTYPE");
                        QDomElement portRef = createSimpleElement(doc, "CONTEXT-R-PORT-REF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+component->getSubName()+"/"+componentModels->at(compIndex).portNames.at(behaviourModels->at(behavIndex).attributePortIndex.at(runIter).at(attrIter)));
                        opRef.appendChild(portRef);
                        clearAttributes();

                        addAttribute("DEST", "CLIENT-SERVER-OPERATION");
                        QDomElement targetRef = createSimpleElement(doc, "TARGET-REQUIRED-OPERATION-REF", &attrName, &attrValue, "/"+rootPackageName+"/Types/"+interfaceModels->at(interIndex).interfaceName+"/"+interfaceModels->at(interIndex).operationName);
                        opRef.appendChild(targetRef);
                        clearAttributes();

                        shortName = createSimpleElement(doc, "TIMEOUT", NULL, NULL, "0.0");
                        callPoint.appendChild(shortName);
                        shortName.clear();
                    }
                    else
                    {
                        QMessageBox::information(this, "Error", "Invalid attribute type " + behaviourModels->at(behavIndex).attributeNames.at(runIter).at(attrIter),  QMessageBox::Ok);
                        return;
                    }
                }

                shortName = createSimpleElement(doc, "SYMBOL", NULL, NULL, behaviourModels->at(behavIndex).runnableNames.at(runIter));
                runEntity.appendChild(shortName);
                shortName.clear();
            }
            QDomElement multipleInst = createSimpleElement(doc, "SUPPORTS-MULTIPLE-INSTANTIATION", NULL, NULL, "false");
            swcInternalBehav.appendChild(multipleInst);

            // Add the implementation of the component (FOR NOW, ONLY C-BASED IMPLEMENTATION IN 'src' FOLDER. TODO)
            addAttribute("UUID", generateUUID());
            QDomElement swImpl = createSimpleElement(doc, "SWC-IMPLEMENTATION", &attrName, &attrValue);
            componentElements.appendChild(swImpl);
            clearAttributes();

            shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, component->getSubName()+"Implementation");
            swImpl.appendChild(shortName);
            shortName.clear();

            QDomElement codeDesc = doc.createElement("CODE-DESCRIPTORS");
            swImpl.appendChild(codeDesc);

            addAttribute("UUID", generateUUID());
            QDomElement code = createSimpleElement(doc, "CODE", &attrName, &attrValue);
            codeDesc.appendChild(code);
            clearAttributes();

            shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, "src");
            code.appendChild(shortName);
            shortName.clear();

            QDomElement artifectDesc = doc.createElement("ARTIFACT-DESCRIPTORS");
            code.appendChild(artifectDesc);

            QDomElement autoObject = doc.createElement("AUTOSAR-ENGINEERING-OBJECT");
            artifectDesc.appendChild(autoObject);

            shortName = createSimpleElement(doc, "SHORT-LABEL", NULL, NULL, "artifactDescriptor");
            autoObject.appendChild(shortName);
            shortName.clear();

            shortName = createSimpleElement(doc, "CATEGORY", NULL, NULL, "artifactDescriptorCategory");
            autoObject.appendChild(shortName);
            shortName.clear();

            shortName = createSimpleElement(doc, "PROGRAMMING-LANGUAGE", NULL, NULL, "C");
            swImpl.appendChild(shortName);
            shortName.clear();

            addAttribute("UUID", generateUUID());
            QDomElement resCons = createSimpleElement(doc, "RESOURCE-CONSUMPTION", &attrName, &attrValue);
            swImpl.appendChild(resCons);
            clearAttributes();

            shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, "resourceConsumption");
            resCons.appendChild(shortName);
            shortName.clear();

            shortName = createSimpleElement(doc, "VENDOR-ID", NULL, NULL, "0");
            swImpl.appendChild(shortName);
            shortName.clear();

            addAttribute("DEST", "SWC-INTERNAL-BEHAVIOR");
            QDomElement behavRef = createSimpleElement(doc, "BEHAVIOR-REF", &attrName, &attrValue, "/"+rootPackageName+"/Components/"+component->getSubName()+"/"+behaviourModels->at(behavIndex).behaviourName);
            swImpl.appendChild(behavRef);
            clearAttributes();
        }
    }
    // Add types package data
    shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, "Types");
    typesPackage.appendChild(shortName);
    shortName.clear();

    QDomElement typesElements = doc.createElement("ELEMENTS");
    typesPackage.appendChild(typesElements);

    // Add each interface and its implementation
    for (int interIter = 0; interIter < interfaceModels->size(); interIter++)
    {
        if (interfaceModels->at(interIter).interfaceType == "clientServer")
        {
            addAttribute("UUID", generateUUID());
            QDomElement csInt = createSimpleElement(doc, "CLIENT-SERVER-INTERFACE", &attrName, &attrValue);
            typesElements.appendChild(csInt);
            clearAttributes();

            shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, interfaceModels->at(interIter).interfaceName);
            csInt.appendChild(shortName);
            shortName.clear();

            shortName = createSimpleElement(doc, "IS-SERVICE", NULL, NULL, "false");
            csInt.appendChild(shortName);
            shortName.clear();

            QDomElement ops = doc.createElement("OPERATIONS");
            csInt.appendChild(ops);

            addAttribute("UUID", generateUUID());
            QDomElement csOp = createSimpleElement(doc, "CLIENT-SERVER-OPERATION", &attrName, &attrValue);
            ops.appendChild(csOp);
            clearAttributes();

            shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, interfaceModels->at(interIter).operationName);
            csOp.appendChild(shortName);
            shortName.clear();

            QDomElement args = doc.createElement("ARGUMENTS");
            csOp.appendChild(args);

            for (int argIter = 0; argIter < interfaceModels->at(interIter).dataNames.size(); argIter++)
            {
                addAttribute("UUID", generateUUID());
                QDomElement argData = createSimpleElement(doc, "ARGUMENT-DATA-PROTOTYPE", &attrName, &attrValue);
                args.appendChild(argData);
                clearAttributes();

                shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, interfaceModels->at(interIter).dataNames.at(argIter));
                argData.appendChild(shortName);
                shortName.clear();

                addAttribute("DEST", "IMPLEMENTATION-DATA-TYPE");
                QDomElement typeRef = createSimpleElement(doc, "TYPE-TREF", &attrName, &attrValue, "/ArcCore/Platform/ImplementationDataTypes/" + interfaceModels->at(interIter).dataType.at(argIter));
                argData.appendChild(typeRef);
                clearAttributes();

                shortName = createSimpleElement(doc, "DIRECTION", NULL, NULL, interfaceModels->at(interIter).dataDirection.at(argIter).toUpper());
                argData.appendChild(shortName);
                shortName.clear();
            }
        }
        else if (interfaceModels->at(interIter).interfaceType == "senderReceiver")
        {
            addAttribute("UUID", generateUUID());
            QDomElement srInt = createSimpleElement(doc, "SENDER-RECEIVER-INTERFACE", &attrName, &attrValue);
            typesElements.appendChild(srInt);
            clearAttributes();

            shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, interfaceModels->at(interIter).interfaceName);
            srInt.appendChild(shortName);
            shortName.clear();

            shortName = createSimpleElement(doc, "IS-SERVICE", NULL, NULL, "false");
            srInt.appendChild(shortName);
            shortName.clear();

            QDomElement dataElem = doc.createElement("DATA-ELEMENTS");
            srInt.appendChild(dataElem);

            for (int argIter = 0; argIter < interfaceModels->at(interIter).dataNames.size(); argIter++)
            {
                addAttribute("UUID", generateUUID());
                QDomElement dataProto = createSimpleElement(doc, "VARIABLE-DATA-PROTOTYPE", &attrName, &attrValue);
                dataElem.appendChild(dataProto);
                clearAttributes();

                shortName = createSimpleElement(doc, "SHORT-NAME", NULL, NULL, interfaceModels->at(interIter).dataNames.at(argIter));
                dataProto.appendChild(shortName);
                shortName.clear();

                QDomElement dataProps = doc.createElement("SW-DATA-DEF-PROPS");
                dataProto.appendChild(dataProps);

                QDomElement dataPropsVariants = doc.createElement("SW-DATA-DEF-PROPS-VARIANTS");
                dataProps.appendChild(dataPropsVariants);

                QDomElement dataPropsCondition = doc.createElement("SW-DATA-DEF-PROPS-CONDITIONAL");
                dataPropsVariants.appendChild(dataPropsCondition);

                shortName = createSimpleElement(doc, "SW-IMPL-POLICY", NULL, NULL, "STANDARD");
                dataPropsCondition.appendChild(shortName);
                shortName.clear();

                addAttribute("DEST", "IMPLEMENTATION-DATA-TYPE");
                QDomElement typeRef = createSimpleElement(doc, "TYPE-TREF", &attrName, &attrValue, "/ArcCore/Platform/ImplementationDataTypes/" + interfaceModels->at(interIter).dataType.at(argIter));
                dataProto.appendChild(typeRef);
                clearAttributes();
            }
        }
        else
        {
            QMessageBox::information(this, "Error", "Unknown interface type " + interfaceModels->at(interIter).interfaceType,  QMessageBox::Ok);
            return;
        }
    }

    // Write data to file
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::information( this, "Error", "Error: Cannot write file. Error: " + file.errorString(), QMessageBox::Ok);
        return;
    }
    doc.setContent(&file);
    QDomProcessingInstruction instr = doc.createProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
    doc.appendChild(instr);
    doc.appendChild(root);
    QTextStream out(&file);
    doc.save(out, 2);
}

QString MainEditor::generateUUID()
{
    uuid_t uuid;
    char uuid_str[37];
    uuid_generate(uuid);
    uuid_unparse(uuid, uuid_str);
    //QString uuidStr(uuid_str);
    return uuid_str;
}

QDomElement MainEditor::createSimpleElement(QDomDocument &doc, const QString &tag, QVector<QString> *attributeNames, QVector<QString> *attributeValues, const QString &value)
{
    QDomElement el = doc.createElement( tag );
    if ( !value.isNull() ) {
      QDomText txt = doc.createTextNode( value );
      el.appendChild( txt );
    }
    if ( attributeNames != NULL )
    {
        for (int i = 0; i < attributeNames->size(); i++)
        {
            el.setAttribute(attributeNames->at(i), attributeValues->at(i));
        }
    }
    return el;
}

void MainEditor::addAttribute(const QString &n, const QString &v)
{
    attrName.push_back(n);
    attrValue.push_back(v);
}

void MainEditor::clearAttributes()
{
    attrName.clear();
    attrValue.clear();
}
