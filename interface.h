/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef INTERFACE_H
#define INTERFACE_H

#include <QGraphicsPathItem>
#include <QPen>

#include "port.h"

class Interface : public QGraphicsPathItem
{
public:
    enum { Type = QGraphicsItem::UserType + 2 };

    Interface(QGraphicsItem *parent = 0);
    ~Interface();

    void setPos1(const QPointF &p);
    void setPos2(const QPointF &p);
    void setPort1(Port *p);
    void setPort2(Port *p);
    void setModifyPath(int value);
    void setFirstX(const qreal &value);
    void setSecondY(const qreal &value);
    void setThirdX(const qreal &value);
    void updatePosFromPorts();
    void updatePath();
    Port* port1() const;
    Port* port2() const;

    void save(QDataStream&);
    void load(QDataStream&, const QMap<quint64, Port*> &portMap);

    int type() const { return Type; }

    int getModifyPath() const;

private:
    QPointF pos1;
    QPointF pos2;
    Port *m_port1;
    Port *m_port2;
    int modifyPath;
    qreal firstX;
    qreal secondY;
    qreal thirdX;
    bool twistedConnection;
    bool pathReset;
};

#endif // INTERFACE_H
