/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef GENERATEEXTRACTDIALOG_H
#define GENERATEEXTRACTDIALOG_H

#include <QDialog>

namespace Ui {
class generateextractdialog;
}

class GenerateExtractDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GenerateExtractDialog(QWidget *parent = 0);
    ~GenerateExtractDialog();
    QStringList getData();

private slots:
    void on_pushButton_clicked();

private:

    bool hasWhiteSpace(QString s);
    void done(int r);

    Ui::generateextractdialog *ui;
};

#endif // GENERATEEXTRACTDIALOG_H
