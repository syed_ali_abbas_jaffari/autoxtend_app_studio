/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "interface.h"

#define DISTANCE_FROM_PORT 50

#include <QtDebug>

Interface::Interface(QGraphicsItem *parent) : QGraphicsPathItem(parent)
{
    setPen(QPen(Qt::black, 2));
    setBrush(Qt::NoBrush);
    setZValue(-1);
    m_port1 = 0;
    m_port2 = 0;
    modifyPath = 0;
    firstX = 0;
    secondY = 0;
    thirdX = 0;
    twistedConnection = false;
    pathReset = false;
}

Interface::~Interface()
{
    if (m_port1)
        m_port1->getConnections().remove(m_port1->getConnections().indexOf(this));
    if (m_port2)
        m_port2->getConnections().remove(m_port2->getConnections().indexOf(this));
}

void Interface::setPos1(const QPointF &p)
{
    pos1 = p;
}

void Interface::setPos2(const QPointF &p)
{
    pos2 = p;
}

void Interface::setPort1(Port *p)
{
    m_port1 = p;

    m_port1->getConnections().append(this);
}

void Interface::setPort2(Port *p)
{
    m_port2 = p;

    m_port2->getConnections().append(this);
}

void Interface::updatePosFromPorts()
{
    pos1 = m_port1->scenePos();
    pos2 = m_port2->scenePos();
}

void Interface::updatePath()
{
    QPainterPath p;

    p.moveTo(pos1);
    if (pathReset)
    {
        if (m_port2 == 0)
        {
            p.lineTo(pos1.x() + (pos2.x()-pos1.x()) / 2, pos1.y());
            p.lineTo(pos2.x() - (pos2.x()-pos1.x()) / 2, pos2.y());
        }
        else
        {
            /*if (m_port1->isRPort())             // input port (i.e. to the left of swc)
            {*/
                if (pos1.x() >= pos2.x())       // port2 is to the left of port1
                {
                    p.lineTo(pos1.x() + (pos2.x()-pos1.x()) / 2, pos1.y());
                    p.lineTo(pos2.x() - (pos2.x()-pos1.x()) / 2, pos2.y());
                    twistedConnection = false;
                }
                else                            // curser is to the right of port1
                {
                    p.lineTo(pos1.x() - DISTANCE_FROM_PORT, pos1.y());
                    p.lineTo(pos1.x() - DISTANCE_FROM_PORT, pos1.y() + (pos2.y()-pos1.y()) / 2);
                    p.lineTo(pos2.x() + DISTANCE_FROM_PORT, pos1.y() + (pos2.y()-pos1.y()) / 2);
                    p.lineTo(pos2.x() + DISTANCE_FROM_PORT, pos2.y());
                    twistedConnection = true;
                }
            /*}
            else
            {
                if (pos2.x() >= pos1.x())          // port2 is to the right of port1
                {
                    p.lineTo(pos1.x() + (pos2.x()-pos1.x()) / 2, pos1.y());
                    p.lineTo(pos2.x() - (pos2.x()-pos1.x()) / 2, pos2.y());
                }
                else                                // port2 is to the left of port1
                {
                    p.lineTo(pos1.x() + DISTANCE_FROM_PORT, pos1.y());
                    p.lineTo(pos1.x() + DISTANCE_FROM_PORT, pos1.y() + (pos2.y()-pos1.y()) / 2);
                    p.lineTo(pos2.x() - DISTANCE_FROM_PORT, pos1.y() + (pos2.y()-pos1.y()) / 2);
                    p.lineTo(pos2.x() - DISTANCE_FROM_PORT, pos2.y());
                    twistedConnection = true;
                }
            }*/
        }
    }
    else
    {
        if (!twistedConnection)
        {
            p.lineTo(firstX, pos1.y());
            p.lineTo(firstX, pos2.y());
            if (pos1.x() <= pos2.x())       // port2 is to the left of port1
            {
                pathReset = true;
            }
        }
        else
        {
            p.lineTo(firstX, pos1.y());
            p.lineTo(firstX, secondY);
            p.lineTo(thirdX, secondY);
            p.lineTo(thirdX, pos2.y());
            if (pos1.x() >= pos2.x())       // port2 is to the left of port1
            {
                pathReset = true;
            }
        }
    }
    p.lineTo(pos2);
    // TODO Check if the earlier created paths are existing. This may lead to out of memory if they exist.
    setPath(p);
}

Port* Interface::port1() const
{
    return m_port1;
}

Port* Interface::port2() const
{
    return m_port2;
}

void Interface::save(QDataStream &ds)
{
    ds << (quint64) m_port1;
    ds << (quint64) m_port2;
}

void Interface::load(QDataStream &ds, const QMap<quint64, Port*> &portMap)
{
    quint64 ptr1;
    quint64 ptr2;
    ds >> ptr1;
    ds >> ptr2;

    setPort1(portMap[ptr1]);
    setPort2(portMap[ptr2]);
    updatePosFromPorts();
    updatePath();
}

void Interface::setFirstX(const qreal &value)
{
    firstX = value;
}

int Interface::getModifyPath() const
{
    return modifyPath;
}

void Interface::setModifyPath(int value)
{
    modifyPath = value;
    pathReset = false;
    // Freeze lines other than clicked one
    if (twistedConnection)
    {
        if (value == 1)
        {
            secondY = path().elementAt(2).y;
            thirdX = path().elementAt(3).x;
        }
        else if (value == 2)
        {
            firstX = path().elementAt(1).x;
            thirdX = path().elementAt(3).x;
        }
        else
        {
            secondY = path().elementAt(2).y;
            firstX = path().elementAt(1).x;
        }
    }
}

void Interface::setSecondY(const qreal &value)
{
    secondY = value;
}

void Interface::setThirdX(const qreal &value)
{
    thirdX = value;
}
