/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef CREATESWCDIALOG_H
#define CREATESWCDIALOG_H

#include <QDialog>
#include <QStandardItemModel>
#include "comboboxdelegate.h"
#include "interfacemodel.h"
#include "swcomponentmodel.h"
#include "behaviourmodel.h"

namespace Ui {
class CreateSWCDialog;
}

class CreateSWCDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateSWCDialog(QWidget *parent = 0);
    ~CreateSWCDialog();

    void setData(QVector<InterfaceModel> *interfaces, QVector <SWComponentModel> *components, QVector<behaviourModel> *beh);
    void done(int);
    SWComponentModel getComponent();
    void editComponent(SWComponentModel comp);

private slots:
    void on_addPortButton_clicked();
    void on_removeButton_clicked();
    bool hasWhiteSpace(QString s);

private:
    Ui::CreateSWCDialog *ui;
    QStandardItemModel *model;
    ComboBoxDelegate *myCombo;
    QVector<InterfaceModel> *interfaces;
    QVector<SWComponentModel> *components;
    QVector<behaviourModel> *behaviors;

    QString edittedComponentName;
};

#endif // CREATESWCDIALOG_H
