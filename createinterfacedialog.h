/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef CREATEINTERFACEDIALOG_H
#define CREATEINTERFACEDIALOG_H

#include <QDialog>
#include <QStandardItemModel>
#include "comboboxdelegate.h"
#include "interfacemodel.h"
#include "swcomponentmodel.h"
#include "behaviourmodel.h"

namespace Ui {
class CreateInterfaceDialog;
}

class CreateInterfaceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateInterfaceDialog(QWidget *parent = 0);
    ~CreateInterfaceDialog();

    void editInterface(InterfaceModel interface);
    void setData(QVector <InterfaceModel> *interfaces, QVector <SWComponentModel> *components, QVector <behaviourModel> *behaviours);
    InterfaceModel getInterface();
    bool hasWhiteSpace (QString s);

private slots:
    void on_addButton_clicked();
    void on_interfaceType_currentIndexChanged(int index);
    void on_removeButton_clicked();
    void done(int);

private:
    Ui::CreateInterfaceDialog *ui;
    QStandardItemModel *model;
    ComboBoxDelegate *myCombo;
    QVector <InterfaceModel> *interfaces;
    QVector <SWComponentModel> *components;
    QVector <behaviourModel> *behaviours;

    QString editInterfaceName;
};

#endif // CREATEINTERFACEDIALOG_H
