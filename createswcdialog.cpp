/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "createswcdialog.h"
#include "ui_createswcdialog.h"
#include <QMessageBox>
#include <QDebug>

void Log (QString);

CreateSWCDialog::CreateSWCDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateSWCDialog)
{
    ui->setupUi(this);
    edittedComponentName = "";
}

CreateSWCDialog::~CreateSWCDialog()
{
    delete ui;
}

void CreateSWCDialog::setData(QVector<InterfaceModel> *interfaces, QVector <SWComponentModel> *components, QVector <behaviourModel> *beh)
{
    QVector <int> indices;
    indices.push_back(1);
    indices.push_back(2);
    QVector < QStringList > strs;
    QStringList column;
    column << "sender" << "receiver" << "client" << "server";
    strs.push_back(column);
    column.clear();
    this->interfaces = interfaces;
    behaviors = beh;
    this->components = components;
    for (int iter = 0; iter < interfaces->size(); iter++)
    {
        column << interfaces->at(iter).interfaceName;
    }
    strs.push_back(column);

    myCombo = new ComboBoxDelegate(this, &indices, &strs);
    ui->tableView->setItemDelegate(myCombo);

    model = new QStandardItemModel(0, 3, this);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Port Name")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Port Type")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Interface Type")));
    ui->tableView->setModel(model);

    ui->tableView->setColumnWidth(0,156);
    ui->tableView->setColumnWidth(1,156);
    ui->tableView->setColumnWidth(2,156);

}

void CreateSWCDialog::done(int r)
{
    // Make sure that the interface model passed to the main editor is valid
    if(QDialog::Accepted == r)  // ok was pressed
    {
        if (model->rowCount() == 0)
        {
            QMessageBox::information(this, "Error", "Number of ports must be greater than 0.", QMessageBox::Ok);
            return;
        }
        // Check if the text fields are valid
        if (ui->componentName->text().isEmpty() || hasWhiteSpace(ui->componentName->text()) || ui->componentName->text().toInt() != 0)
        {
            QMessageBox::information(this, "Error", "Please type in a valid component name. Whitespaces/numbered-names are not allowed.", QMessageBox::Ok);
            return;
        }
        QString value1, value2;
        for (int dataIter = 0; dataIter < model->rowCount(); dataIter++)
        {
            value1 = model->data(model->index(dataIter, 0)).toString();
            if (value1.isEmpty() || value1.toInt() != 0 || hasWhiteSpace(value1))
            {
                QMessageBox::information(this, "Error", "Please type in a valid port name. Whitespaces/numbered-names are not allowed.", QMessageBox::Ok);
                return;
            }
            // Check if sender receiver ports access only senderReceiver interface and same for client server ports
            value1 = model->data(model->index(dataIter, 1)).toString();
            value2 = model->data(model->index(dataIter, 2)).toString();
            for (int intIter = 0; intIter < interfaces->size(); intIter++)
            {
                if (interfaces->at(intIter).interfaceName == value2)
                {
                    if (interfaces->at(intIter).interfaceType == "senderReceiver" && (value1 == "client" || value1 == "server"))
                    {
                        QMessageBox::information(this, "Error", "Interface \'" + interfaces->at(intIter).interfaceName + "\' is of senderReceiver type. It is not compatible with client/server ports.", QMessageBox::Ok);
                        return;
                    }
                    if (interfaces->at(intIter).interfaceType == "clientServer" && (value1 == "sender" || value1 == "receiver"))
                    {
                        QMessageBox::information(this, "Error", "Interface \'" + interfaces->at(intIter).interfaceName + "\' is of clientServer type. It is not compatible with sender/receiver ports.", QMessageBox::Ok);
                        return;
                    }
                    break;
                }
            }
            // Check if the port names are unique
            for (int dataIter2 = 0; dataIter2 < model->rowCount(); dataIter2++)
            {
                if (dataIter2 != dataIter && model->data(model->index(dataIter,0)).toString() == model->data(model->index(dataIter2, 0)).toString())
                {
                    QMessageBox::information(this, "Error", "Port names must be unique.", QMessageBox::Ok);
                    return;
                }
            }
        }
        // Check if the component name is unique
        for (int compIter = 0; compIter < components->size(); compIter++)
        {
            if (components->at(compIter).componentName == ui->componentName->text() && ui->componentName->text() != edittedComponentName)
            {
                QMessageBox::information(this, "Error", "Component name must be unique.", QMessageBox::Ok);
                return;
            }
        }

        QDialog::done(r);
        return;
    }
    else    // cancel, close or exc was pressed
    {
         QDialog::done(r);
         return;
    }
}

SWComponentModel CreateSWCDialog::getComponent()
{
    SWComponentModel newSWC;
    newSWC.interfaces = interfaces;
    newSWC.componentName = ui->componentName->text();
    newSWC.componentType = ui->componentType->currentText();
    for (int rowIter = 0; rowIter < model->rowCount(); rowIter++)
    {
        newSWC.portNames.push_back(model->data(model->index(rowIter, 0)).toString());
        newSWC.portType.push_back(model->data(model->index(rowIter, 1)).toString());
        for (int elIter = 0; elIter < interfaces->size(); elIter++)
        {
            if (model->data(model->index(rowIter, 2)).toString() == interfaces->at(elIter).interfaceName)
            {
                newSWC.portInterfaceTypeIndex.push_back(elIter);
                break;
            }
        }
    }

    Log ("Getting component " + ui->componentName->text());
    Log ("\tComponent Type " + ui->componentType->currentText());
    for (int rowIter = 0; rowIter < model->rowCount(); rowIter++)
    {
        Log("\t\tPort Name " + model->data(model->index(rowIter, 0)).toString());
        Log("\t\tPort Type " + model->data(model->index(rowIter, 1)).toString());
        Log("\t\tPort Interface Type " + model->data(model->index(rowIter, 2)).toString());
    }
    return newSWC;
}

void CreateSWCDialog::on_addPortButton_clicked()
{
    QStandardItem *name = new QStandardItem(QString(""));
    QStandardItem *type = new QStandardItem(QString("sender"));
    QStandardItem *intName = new QStandardItem(interfaces->at(0).interfaceName);
    QList<QStandardItem*> row;
    row << name << type << intName;
    model->appendRow(row);
}

void CreateSWCDialog::on_removeButton_clicked()
{
    if (edittedComponentName != "")
    {
        Log ("Removing port " + model->data(model->index(ui->tableView->currentIndex().row(), 0)).toString());
    }
    if (model->rowCount() > 1)
    {
        int behaviourIndex = -1;
        for (int behIter = 0; behIter < behaviors->size(); behIter++)
        {
            if ((*behaviors)[behIter].getComponentType() == ui->componentName->text())
            {
                for (int runIter = 0; runIter < behaviors->at(behIter).runnableNames.size(); runIter++)
                {
                    for (int attIter = 0; attIter < behaviors->at(behIter).attributeNames.at(runIter).size(); attIter++)
                    {
                        if (model->data(model->index(ui->tableView->currentIndex().row(), 0)).toString() == (*behaviors)[behIter].getAttributePort(runIter, attIter))
                        {
                            behaviourIndex = behIter;
                            break;
                        }
                    }
                }
                break;
            }
        }
        QMessageBox::StandardButton reply1;
        if (behaviourIndex != -1)
        {
            reply1 = QMessageBox::question(this, "Confirm", "Internal behaviour \'" + behaviors->at(behaviourIndex).behaviourName + "\' uses this port in some runnable. Removing this port may lead to removal of related attributes. Are you sure you want to remove this port?", QMessageBox::Yes | QMessageBox::No);
            if (reply1 == QMessageBox::Yes)
            {
                // Remove the attributes
                for (int runIter = 0; runIter < behaviors->at(behaviourIndex).runnableNames.size(); runIter++)
                {
                    for (int attIter = 0; attIter < behaviors->at(behaviourIndex).attributeNames.at(runIter).size(); attIter++)
                    {
                        if (model->data(model->index(ui->tableView->currentIndex().row(), 0)).toString() == (*behaviors)[behaviourIndex].getAttributePort(runIter, attIter))
                        {
                            (*behaviors)[behaviourIndex].removeAttribute(runIter, attIter);
                            attIter--;
                        }
                    }
                }
                // Fix attributes' port indices
                for (int runIter = 0; runIter < behaviors->at(behaviourIndex).runnableNames.size(); runIter++)
                {
                    for (int attIter = 0; attIter < behaviors->at(behaviourIndex).attributeNames.at(runIter).size(); attIter++)
                    {
                        if ((*behaviors)[behaviourIndex].attributePortIndex[runIter][attIter] >= ui->tableView->currentIndex().row())
                        {
                            (*behaviors)[behaviourIndex].attributePortIndex[runIter][attIter]--;
                        }
                    }
                }
                model->removeRows(ui->tableView->currentIndex().row(), 1);
            }
        }
        else
        {
            reply1 = QMessageBox::question(this, "Confirm", "Are you sure you want to remove this port?", QMessageBox::Yes|QMessageBox::No);
            if (reply1 == QMessageBox::Yes)
            {
                model->removeRows(ui->tableView->currentIndex().row(), 1);
            }
        }
    }
}

bool CreateSWCDialog::hasWhiteSpace(QString s)
{
    std::string str = s.toStdString();
    for (int cIter = 0; cIter < s.size(); cIter++)
    {
        if (isspace(str[cIter]))
        {
            return true;
        }
    }
    return false;
}

void CreateSWCDialog::editComponent(SWComponentModel comp)
{
    edittedComponentName = comp.componentName;
    ui->componentName->setText(comp.componentName);
    int editable = true;
    for (int behIter = 0; behIter < behaviors->size(); behIter++)
    {
        if (components->at(behaviors->at(behIter).componentTypeIndex).componentName == comp.componentName)
        {
            editable = false;
            break;
        }
    }
    if (comp.componentType == "application")
        ui->componentType->setCurrentIndex(0);
    for (int dataIter = 0; dataIter < comp.portNames.size(); dataIter++)
    {
        QStandardItem *name = new QStandardItem(QString(comp.portNames.at(dataIter)));
        QStandardItem *type = new QStandardItem(QString(comp.portType.at(dataIter)));
        QStandardItem *intType = new QStandardItem(QString(comp.getInterfaceType(dataIter)));
        if (!editable)
        {
            type->setEditable(false);
            intType->setEditable(false);
        }
        QList<QStandardItem*> row;
        row << name << type << intType;
        model->appendRow(row);
    }
}
