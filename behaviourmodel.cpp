/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "behaviourmodel.h"

#include <QMessageBox>
#include <QDebug>

behaviourModel::behaviourModel()
{
    interfaces = NULL;
    components = NULL;
}

QString behaviourModel::getComponentType(void)                                      // i.e. Component's name
{
    if (components == NULL)
    {
        //QMessageBox::information(this, "Error", "Components not initialized.", QMessageBox::Ok);
        qDebug() << "Components not initialized";
        return "";
    }
    if (componentTypeIndex >= components->size())
    {
        //QMessageBox::information(this, "Error", "Component index is invalid.", QMessageBox::Ok);
        qDebug() << "Component index is invalid";
        return "";
    }
    return components->at(componentTypeIndex).componentName;
}

QString behaviourModel::getAttributePort(int runnable, int attribute)           // i.e. Port name from the component
{
    if (components == NULL)
    {
        //QMessageBox::information(this, "Error", "Components not initialized.", QMessageBox::Ok);
        qDebug() << "Components not initialized";
        return "";
    }
    if (componentTypeIndex >= components->size())
    {
        //QMessageBox::information(this, "Error", "Component index is invalid.", QMessageBox::Ok);
        qDebug() << "Component index is invalid";
        return "";
    }
    if (runnable >= attributePortIndex.size())
    {
        //QMessageBox::information(this, "Error", "Runnable index is invalid.", QMessageBox::Ok);
        qDebug() << "Runnable index is invalid";
        return "";
    }
    if (attribute >= attributePortIndex[runnable].size())
    {
        //QMessageBox::information(this, "Error", "Attribute index is invalid.", QMessageBox::Ok);
        qDebug() << "Attribute index is invalid";
        return "";
    }

    return components->at(componentTypeIndex).portNames.at(attributePortIndex[runnable][attribute]);
}

QString behaviourModel::getAttributeElement(int runnable, int attribute)       // i.e. Data name from interface
{
    if (interfaces == NULL)
    {
        //QMessageBox::information(this, "Error", "Interfaces not initialized.", QMessageBox::Ok);
        qDebug() << "Interfaces not initialized";
        return "";
    }
    if (components == NULL)
    {
        //QMessageBox::information(this, "Error", "Components not initialized.", QMessageBox::Ok);
        qDebug() << "Components not initialized";
        return "";
    }
    if (componentTypeIndex >= components->size())
    {
        //QMessageBox::information(this, "Error", "Component index is invalid.", QMessageBox::Ok);
        qDebug() << "Component index is invalid";
        return "";
    }
    if (runnable >= attributeElementIndex.size())
    {
        //QMessageBox::information(this, "Error", "Runnable index is invalid.", QMessageBox::Ok);
        qDebug() << "Runnable index is invalid";
        return "";
    }
    if (attribute >= attributeElementIndex[runnable].size())
    {
        //QMessageBox::information(this, "Error", "Attribute index is invalid.", QMessageBox::Ok);
        qDebug() << "Attribute index is invalid";
        return "";
    }
    if (components->at(componentTypeIndex).portInterfaceTypeIndex.at(attributePortIndex[runnable][attribute]) > interfaces->size())
    {
        //QMessageBox::information(this, "Error", "Port interface index is invalid.", QMessageBox::Ok);
        qDebug() << "Port interface index is invalid";
        return "";
    }
    if (attributeElementIndex[runnable][attribute] >= interfaces->at(components->at(componentTypeIndex).portInterfaceTypeIndex.at(attributePortIndex[runnable][attribute])).dataNames.size())
    {
        //QMessageBox::information(this, "Error", "Attribute index is invalid.", QMessageBox::Ok);
        qDebug() << "AttributeElementIndex out of bound";
        return "";
    }
    return interfaces->at(components->at(componentTypeIndex).portInterfaceTypeIndex.at(attributePortIndex[runnable][attribute])).dataNames.at(attributeElementIndex[runnable][attribute]);
}

QString behaviourModel::getInvokeEventPort(int runnable)                        // i.e. Port name from the component
{
    if (components == NULL)
    {
        //QMessageBox::information(this, "Error", "Components not initialized.", QMessageBox::Ok);
        qDebug() << "Components not initialized";
        return "";
    }
    if (componentTypeIndex >= components->size())
    {
        //QMessageBox::information(this, "Error", "Component index is invalid.", QMessageBox::Ok);
        qDebug() << "Component index is invalid";
        return "";
    }
    if (runnable >= iEventPortIndex.size())
    {
        //QMessageBox::information(this, "Error", "Runnable index is invalid.", QMessageBox::Ok);
        qDebug() << "Runnable index is invalid";
        return "";
    }
    return components->at(componentTypeIndex).portNames.at(iEventPortIndex[runnable]);
}

QString behaviourModel::getInvokeEventOperation(int runnable)                   // i.e. Operation name from the interface
{
    if (interfaces == NULL)
    {
        //QMessageBox::information(this, "Error", "Interfaces not initialized.", QMessageBox::Ok);
        qDebug() << "Interfaces not initialized";
        return "";
    }
    if (components == NULL)
    {
        //QMessageBox::information(this, "Error", "Components not initialized.", QMessageBox::Ok);
        qDebug() << "Components not initialized";
        return "";
    }
    if (componentTypeIndex >= components->size())
    {
        //QMessageBox::information(this, "Error", "Component index is invalid.", QMessageBox::Ok);
        qDebug() << "Component index is invalid";
        return "";
    }
    if (runnable >= iEventPortIndex.size())
    {
        //QMessageBox::information(this, "Error", "Runnable index is invalid.", QMessageBox::Ok);
        qDebug() << "Runnable index is invalid";
        return "";
    }
    if (components->at(componentTypeIndex).portInterfaceTypeIndex.at(iEventPortIndex[runnable]) > interfaces->size())
    {
        //QMessageBox::information(this, "Error", "Port interface index is invalid.", QMessageBox::Ok);
        qDebug() << "Port interface index is invalid";
        return "";
    }
    return interfaces->at(components->at(componentTypeIndex).portInterfaceTypeIndex.at(iEventPortIndex[runnable])).operationName;
}

void behaviourModel::removeAttribute(int runnableIndex, int attributeIndex)
{
    attributeNames[runnableIndex].remove(attributeIndex);
    attributePortIndex[runnableIndex].remove(attributeIndex);
    attributeElementIndex[runnableIndex].remove(attributeIndex);
}

