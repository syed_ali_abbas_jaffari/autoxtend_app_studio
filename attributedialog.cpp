/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#include "attributedialog.h"
#include "ui_attributedialog.h"

#include <QMessageBox>

AttributeDialog::AttributeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AttributeDialog)
{
    ui->setupUi(this);
}

AttributeDialog::~AttributeDialog()
{
    delete ui;
}

void AttributeDialog::setData(QVector <InterfaceModel> *interfaces, SWComponentModel *component)
{
    this->interfaces = interfaces;
    this->component = component;

    for (int pIter = 0; pIter < component->portNames.size(); pIter++)
    {
        if (component->portType[pIter] == "receiver")
            ui->portName->addItem(component->portNames[pIter]);
    }
}

void AttributeDialog::getData(QString *attributeName, QString *portName, QString *elementName)
{
    *attributeName = ui->attribute->currentText();
    *portName = ui->portName->currentText();
    *elementName = ui->dataElementName->currentText();
}

void AttributeDialog::editAttribute(QString attributeName, QString portName, QString elementName)
{
    for (int nIter = 0; nIter < ui->attribute->count(); nIter++)
    {
        if (ui->attribute->itemText(nIter) == attributeName)
        {
            ui->attribute->setCurrentIndex(nIter);
            break;
        }
    }
    for (int pIter = 0; pIter < ui->portName->count(); pIter++)
    {
        if (ui->portName->itemText(pIter) == portName)
        {
            ui->portName->setCurrentIndex(pIter);
            break;
        }
    }
    for (int eIter = 0; eIter < ui->dataElementName->count(); eIter++)
    {
        if (ui->dataElementName->itemText(eIter) == elementName)
        {
            ui->dataElementName->setCurrentIndex(eIter);
            break;
        }
    }
}

void AttributeDialog::done(int r)
{
    // Make sure that the attribute passed to the internalBehaviourDialog is valid
    if(QDialog::Accepted == r)  // ok was pressed
    {
        if (ui->portName->currentText() == "" || ui->dataElementName->currentText() == "")
        {
            QMessageBox::information(this, "Error", "Not a valid attribute. All fields must be filled.", QMessageBox::Ok);
            return;
        }
        QDialog::done(r);
        return;
    }
    else    // cancel, close or exc was pressed
    {
         QDialog::done(r);
         return;
    }
}

void AttributeDialog::on_portName_currentIndexChanged(int index)
{
    Q_UNUSED(index);
    ui->dataElementName->clear();
    if (ui->attribute->currentIndex() != 2)
    {
        for (int pIter = 0; pIter < component->portNames.size(); pIter++)
        {
            if (component->portNames.at(pIter) == ui->portName->currentText())
            {
                for (int iIter = 0; iIter < interfaces->size(); iIter++)
                {
                    if (interfaces->at(iIter).interfaceName == component->getInterfaceType(pIter))
                    {
                        for (int elIter = 0; elIter < interfaces->at(iIter).dataNames.size(); elIter++)
                        {
                            ui->dataElementName->addItem(interfaces->at(iIter).dataNames.at(elIter));
                        }
                        break;
                    }
                }
                break;
            }
        }
        if (ui->dataElementName->count() > 1)
        {
            ui->dataElementName->addItem("*");
        }
    }
    else
    {
        for (int pIter = 0; pIter < component->portNames.size(); pIter++)
        {
            if (component->portNames.at(pIter) == ui->portName->currentText())
            {
                for (int iIter = 0; iIter < interfaces->size(); iIter++)
                {
                    if (interfaces->at(iIter).interfaceName == component->getInterfaceType(pIter))
                    {
                        ui->dataElementName->addItem(interfaces->at(iIter).operationName);
                        break;
                    }
                }
                break;
            }
        }
    }
}

void AttributeDialog::on_attribute_currentIndexChanged(int index)
{
    ui->portName->clear();
    for (int pIter = 0; pIter < component->portNames.size(); pIter++)
    {
        if (index == 0)
        {
            if (component->portType[pIter] == "receiver")
                ui->portName->addItem(component->portNames[pIter]);
        }
        else if (index == 1)
        {
            if (component->portType[pIter] == "sender")
                ui->portName->addItem(component->portNames[pIter]);
        }
        else if (index == 2)
        {
            if (component->portType[pIter] == "client")
                ui->portName->addItem(component->portNames[pIter]);
        }
    }
}
