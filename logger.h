/* ----------------------------- AUTOSAR App Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR App Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR App Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR App Studio ---------------------------*/

#ifndef LOGGER_H
#define LOGGER_H

#include <QFile>
#include <QTextStream>

void Log(QString str)
{
    QFile logFile("log.txt");

    if (!logFile.open(QFile::Text | QFile::Append))
    {
        return;
    }

    QTextStream stream(&logFile);
    stream << str << "\n";
    logFile.flush();
    logFile.close();
}


#endif // LOGGER_H

